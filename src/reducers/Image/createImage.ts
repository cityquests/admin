import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostImageAction } from '../../actions';
import { Image } from '../../models';

export interface CreateImageState extends Image {
  isFetching: boolean;
}

const initialState: CreateImageState = {
  isFetching: false,
  _id: '',
  imageId: '',
  url: '',
  areaId: 0,
  typeId: 0,
  location: [],
  name: '',
};

export function createImage(
  state: CreateImageState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostImageAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostImageAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchPostImageAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
