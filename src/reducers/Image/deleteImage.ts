import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteImageAction } from '../../actions';

export interface DeleteImageState {
  isFetching: boolean;
}

const initialState: DeleteImageState = {
  isFetching: false,
};

export function deleteImage(
  state: DeleteImageState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteImageAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteImageAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteImageAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
