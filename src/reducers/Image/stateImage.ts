import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetImageAction } from '../../actions';
import { Image } from '../../models';

export interface ImageState extends Image {
  isFetching: boolean;
}

const initialState: ImageState = {
  isFetching: true,
  _id: '',
  imageId: '',
  url: '',
  areaId: 0,
  typeId: 0,
  location: [],
  name: '',
};

export function stateImage(
  state: ImageState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetImageAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetImageAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetImageAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
