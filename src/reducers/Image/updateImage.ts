import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateImageAction } from '../../actions';
import { Image } from '../../models';

export interface UpdateImageState extends Image {
  isFetching: boolean;
}

const initialState: UpdateImageState = {
  isFetching: true,
  _id: '',
  imageId: '',
  url: '',
  areaId: 0,
  typeId: 0,
  location: [],
  name: '',
};

export function updateImage(
  state: UpdateImageState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateImageAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateImageAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateImageAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
