import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetImagesAction } from '../../actions';
import { PaginatedImages } from '../../models';

export interface ImagesState extends PaginatedImages {
  isFetching: boolean;
}

const initialState: ImagesState = {
  isFetching: true,
  imagesList: [],
  currentPage: 1,
  pages: 1,
  numOfResults: 0,
};

export function stateImages(
  state: ImagesState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetImagesAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetImagesAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetImagesAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
