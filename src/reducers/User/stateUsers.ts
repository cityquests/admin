import { getType, PayloadAction } from "typesafe-actions";
import { fetchGetUsersAction } from "../../actions";
import { User } from "../../models";

export interface UsersState {
  isFetching: boolean;
  users_list: User[];
}

const initialState: UsersState = {
  isFetching: true,
  users_list: []
};

export function stateUsers(
  state: UsersState = initialState,
  action: PayloadAction<string, any>
) {
  switch (action.type) {
    case getType(fetchGetUsersAction.request):
      return {
        ...state,
        isFetching: true
      };

    case getType(fetchGetUsersAction.success):
      return {
        ...state,
        users_list: action.payload,
        isFetching: false
      };

    case getType(fetchGetUsersAction.failure):
      return {
        ...state,
        isFetching: false
      };

    default:
      return state;
  }
}
