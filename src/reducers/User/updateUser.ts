import { getType, PayloadAction } from "typesafe-actions";
import { fetchUpdateUserAction } from "../../actions";
import { User } from "../../models";

export interface UpdateUserState extends User {
  isFetching: boolean;
}

const initialState: UpdateUserState = {
  isFetching: true,
  description: "",
  email: "",
  password: "",
  phone: "",
  createdAt: "",
  lastLogin: "",
  fullname: "",
  nickname: "",
  birthday: ""
};

export function updateUser(
  state: UpdateUserState = initialState,
  action: PayloadAction<string, any>
) {
  switch (action.type) {
    case getType(fetchUpdateUserAction.request):
      return {
        ...state,
        isFetching: true
      };

    case getType(fetchUpdateUserAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false
      };

    case getType(fetchUpdateUserAction.failure):
      return {
        ...state,
        isFetching: false
      };

    default:
      return state;
  }
}
