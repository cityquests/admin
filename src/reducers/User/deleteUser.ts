import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteUserAction } from '../../actions';

export interface DeleteUserState {
  isFetching: boolean;
}

const initialState: DeleteUserState = {
  isFetching: true,
};

export function deleteUser(
  state: DeleteUserState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteUserAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteUserAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteUserAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
