import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostUserAction } from '../../actions';
import { User } from '../../models';

export interface CreateUserState extends User {
  isFetching: boolean;
}

const initialState: CreateUserState = {
  isFetching: true,
  description: '',
  email: '',
  password: '',
  phone: '',
  createdAt: '',
  lastLogin: '',
  fullname: '',
  nickname: '',
  birthday: '',
};

export function createUser(
  state: CreateUserState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostUserAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostUserAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostUserAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
