import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestionAreasAction } from '../../actions';
import { QuestionArea } from '../../models';

export interface QuestionAreasState {
  isFetching: boolean;
  question_area_list: QuestionArea[];
}

const initialState: QuestionAreasState = {
  isFetching: true,
  question_area_list: [],
};

export function stateQuestionAreas(
  state: QuestionAreasState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestionAreasAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestionAreasAction.success):
      return {
        ...state,
        question_area_list: action.payload,
        isFetching: false,
      };

    case getType(fetchGetQuestionAreasAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
