import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateQuestionAreaAction } from '../../actions';
import { QuestionArea } from '../../models';

export interface UpdateQuestionAreaState extends QuestionArea {
  isFetching: boolean;
}

const initialState: UpdateQuestionAreaState = {
  isFetching: true,
  areaId: 0,
  name: '',
};

export function updateQuestionArea(
  state: UpdateQuestionAreaState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateQuestionAreaAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateQuestionAreaAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateQuestionAreaAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
