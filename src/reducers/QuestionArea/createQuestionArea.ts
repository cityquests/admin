import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostQuestionAreaAction } from '../../actions';
import { QuestionArea } from '../../models';

export interface CreateQuestionAreaState extends QuestionArea {
  isFetching: boolean;
}

const initialState: CreateQuestionAreaState = {
  isFetching: true,
  areaId: 0,
  name: '',
};

export function createQuestionArea(
  state: CreateQuestionAreaState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostQuestionAreaAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostQuestionAreaAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostQuestionAreaAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
