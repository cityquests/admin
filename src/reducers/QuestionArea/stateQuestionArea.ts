import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestionAreaAction } from '../../actions';
import { QuestionArea } from '../../models';

export interface QuestionAreaState extends QuestionArea {
  isFetching: boolean;
}

const initialState: QuestionAreaState = {
  isFetching: true,
  areaId: 0,
  name: '',
};

export function stateQuestionArea(
  state: QuestionAreaState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestionAreaAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestionAreaAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetQuestionAreaAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
