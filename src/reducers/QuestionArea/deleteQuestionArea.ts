import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteQuestionAreaAction } from '../../actions';

export interface DeleteQuestionAreaState {
  isFetching: boolean;
}

const initialState: DeleteQuestionAreaState = {
  isFetching: true,
};

export function deleteQuestionArea(
  state: DeleteQuestionAreaState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteQuestionAreaAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteQuestionAreaAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteQuestionAreaAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
