import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateQuestionTypeAction } from '../../actions';
import { QuestionType } from '../../models';

export interface UpdateQuestionTypeState extends QuestionType {
  isFetching: boolean;
}

const initialState: UpdateQuestionTypeState = {
  isFetching: true,
  typeId: 0,
  name: '',
};

export function updateQuestionType(
  state: UpdateQuestionTypeState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateQuestionTypeAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateQuestionTypeAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateQuestionTypeAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
