import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteQuestionTypeAction } from '../../actions';

export interface DeleteQuestionTypeState {
  isFetching: boolean;
}

const initialState: DeleteQuestionTypeState = {
  isFetching: true,
};

export function deleteQuestionType(
  state: DeleteQuestionTypeState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteQuestionTypeAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteQuestionTypeAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteQuestionTypeAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
