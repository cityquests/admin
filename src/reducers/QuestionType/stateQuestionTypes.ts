import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestionTypesAction } from '../../actions';
import { QuestionType } from '../../models';

export interface QuestionTypesState {
  isFetching: boolean;
  question_type_list: QuestionType[];
}

const initialState: QuestionTypesState = {
  isFetching: true,
  question_type_list: [],
};

export function stateQuestionTypes(
  state: QuestionTypesState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestionTypesAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestionTypesAction.success):
      return {
        ...state,
        question_type_list: action.payload,
        isFetching: false,
      };

    case getType(fetchGetQuestionTypesAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
