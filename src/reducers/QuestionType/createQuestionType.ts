import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostQuestionTypeAction } from '../../actions';
import { QuestionType } from '../../models';

export interface CreateQuestionTypeState extends QuestionType {
  isFetching: boolean;
}

const initialState: CreateQuestionTypeState = {
  isFetching: true,
  typeId: 0,
  name: '',
};

export function createQuestionType(
  state: CreateQuestionTypeState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostQuestionTypeAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostQuestionTypeAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostQuestionTypeAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
