import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestionTypeAction } from '../../actions';
import { QuestionType } from '../../models';

export interface QuestionTypeState extends QuestionType {
  isFetching: boolean;
}

const initialState: QuestionTypeState = {
  isFetching: true,
  typeId: 0,
  name: '',
};

export function stateQuestionType(
  state: QuestionTypeState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestionTypeAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestionTypeAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetQuestionTypeAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
