import { combineReducers } from 'redux';
import { createUser, CreateUserState } from './User/createUser';
import { deleteUser, DeleteUserState } from './User/deleteUser';
import { stateUser, UserState } from './User/stateUser';
import { stateUsers, UsersState } from './User/stateUsers';
import { updateUser, UpdateUserState } from './User/updateUser';

import { createQuest, CreateQuestState } from './Quest/createQuest';
import { deleteQuest, DeleteQuestState } from './Quest/deleteQuest';
import { stateQuest, QuestState } from './Quest/stateQuest';
import { stateQuests, QuestsState } from './Quest/stateQuests';
import { updateQuest, UpdateQuestState } from './Quest/updateQuest';

import { createQuestion, CreateQuestionState } from './Question/createQuestion';
import { deleteQuestion, DeleteQuestionState } from './Question/deleteQuestion';
import { stateQuestion, QuestionState } from './Question/stateQuestion';
import { stateQuestions, QuestionsState } from './Question/stateQuestions';
import { updateQuestion, UpdateQuestionState } from './Question/updateQuestion';

import { createHint, CreateHintState } from './Hint/createHint';
import { deleteHint, DeleteHintState } from './Hint/deleteHint';
import { stateHint, HintState } from './Hint/stateHint';
import { stateHints, HintsState } from './Hint/stateHints';
import { updateHint, UpdateHintState } from './Hint/updateHint';

import { createStaff, CreateStaffState } from './Staff/createStaff';
import { deleteStaff, DeleteStaffState } from './Staff/deleteStaff';
import { stateStaff, StaffState } from './Staff/stateStaff';
import { stateStaffs, StaffsState } from './Staff/stateStaffs';
import { updateStaff, UpdateStaffState } from './Staff/updateStaff';

import { createImage, CreateImageState } from './Image/createImage';
import { deleteImage, DeleteImageState } from './Image/deleteImage';
import { stateImage, ImageState } from './Image/stateImage';
import { stateImages, ImagesState } from './Image/stateImages';
import { updateImage, UpdateImageState } from './Image/updateImage';

import {
  createQuestionArea,
  CreateQuestionAreaState,
} from './QuestionArea/createQuestionArea';
import {
  deleteQuestionArea,
  DeleteQuestionAreaState,
} from './QuestionArea/deleteQuestionArea';
import {
  stateQuestionArea,
  QuestionAreaState,
} from './QuestionArea/stateQuestionArea';
import {
  stateQuestionAreas,
  QuestionAreasState,
} from './QuestionArea/stateQuestionAreas';
import {
  updateQuestionArea,
  UpdateQuestionAreaState,
} from './QuestionArea/updateQuestionArea';

import {
  createQuestionType,
  CreateQuestionTypeState,
} from './QuestionType/createQuestionType';
import {
  deleteQuestionType,
  DeleteQuestionTypeState,
} from './QuestionType/deleteQuestionType';
import {
  stateQuestionType,
  QuestionTypeState,
} from './QuestionType/stateQuestionType';
import {
  stateQuestionTypes,
  QuestionTypesState,
} from './QuestionType/stateQuestionTypes';
import {
  updateQuestionType,
  UpdateQuestionTypeState,
} from './QuestionType/updateQuestionType';

import {
  createStaffCategory,
  CreateStaffCategoryState,
} from './StaffCategory/createStaffCategory';
import {
  deleteStaffCategory,
  DeleteStaffCategoryState,
} from './StaffCategory/deleteStaffCategory';
import {
  stateStaffCategory,
  StaffCategoryState,
} from './StaffCategory/stateStaffCategory';
import {
  stateStaffCategories,
  StaffCategoriesState,
} from './StaffCategory/stateStaffCategories';
import {
  updateStaffCategory,
  UpdateStaffCategoryState,
} from './StaffCategory/updateStaffCategory';

import { statePermissions, PermissionsState } from './Permissions/statePermissions';

export interface State {
  createUser: CreateUserState;
  deleteUser: DeleteUserState;
  stateUser: UserState;
  stateUsers: UsersState;
  updateUser: UpdateUserState;

  createQuest: CreateQuestState;
  deleteQuest: DeleteQuestState;
  stateQuest: QuestState;
  stateQuests: QuestsState;
  updateQuest: UpdateQuestState;

  createQuestion: CreateQuestionState;
  deleteQuestion: DeleteQuestionState;
  stateQuestion: QuestionState;
  stateQuestions: QuestionsState;
  updateQuestion: UpdateQuestionState;

  createHint: CreateHintState;
  deleteHint: DeleteHintState;
  stateHint: HintState;
  stateHints: HintsState;
  updateHint: UpdateHintState;

  createStaff: CreateStaffState;
  deleteStaff: DeleteStaffState;
  stateStaff: StaffState;
  stateStaffs: StaffsState;
  updateStaff: UpdateStaffState;

  createStaffCategory: CreateStaffCategoryState;
  deleteStaffCategory: DeleteStaffCategoryState;
  stateStaffCategory: StaffCategoryState;
  stateStaffCategories: StaffCategoriesState;
  updateStaffCategory: UpdateStaffCategoryState;

  createImage: CreateImageState;
  deleteImage: DeleteImageState;
  stateImage: ImageState;
  stateImages: ImagesState;
  updateImage: UpdateImageState;

  createQuestionArea: CreateQuestionAreaState;
  deleteQuestionArea: DeleteQuestionAreaState;
  stateQuestionArea: QuestionAreaState;
  stateQuestionAreas: QuestionAreasState;
  updateQuestionArea: UpdateQuestionAreaState;

  createQuestionType: CreateQuestionTypeState;
  deleteQuestionType: DeleteQuestionTypeState;
  stateQuestionType: QuestionTypeState;
  stateQuestionTypes: QuestionTypesState;
  updateQuestionType: UpdateQuestionTypeState;

  statePermissions: PermissionsState;
}

const rootReducer = combineReducers({
  createUser,
  deleteUser,
  stateUser,
  stateUsers,
  updateUser,

  createQuest,
  deleteQuest,
  stateQuest,
  stateQuests,
  updateQuest,

  createQuestion,
  deleteQuestion,
  stateQuestion,
  stateQuestions,
  updateQuestion,

  createHint,
  deleteHint,
  stateHint,
  stateHints,
  updateHint,

  createStaff,
  deleteStaff,
  stateStaff,
  stateStaffs,
  updateStaff,

  createStaffCategory,
  deleteStaffCategory,
  stateStaffCategory,
  stateStaffCategories,
  updateStaffCategory,

  createImage,
  deleteImage,
  stateImage,
  stateImages,
  updateImage,

  createQuestionArea,
  deleteQuestionArea,
  stateQuestionArea,
  stateQuestionAreas,
  updateQuestionArea,

  createQuestionType,
  deleteQuestionType,
  stateQuestionType,
  stateQuestionTypes,
  updateQuestionType,

  statePermissions,
});

export default rootReducer;
