import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostStaffCategoryAction } from '../../actions';
import { StaffCategory } from '../../models';

export interface CreateStaffCategoryState extends StaffCategory {
  isFetching: boolean;
}

const initialState: CreateStaffCategoryState = {
  isFetching: true,
  type: '',
  displayName: '',
  privileges: 0,
};

export function createStaffCategory(
  state: CreateStaffCategoryState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostStaffCategoryAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostStaffCategoryAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostStaffCategoryAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
