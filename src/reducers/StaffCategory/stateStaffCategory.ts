import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetStaffCategoryAction } from '../../actions';
import { StaffCategory } from '../../models';

export interface StaffCategoryState extends StaffCategory {
  isFetching: boolean;
}

const initialState: StaffCategoryState = {
  isFetching: true,
  type: '',
  displayName: '',
  privileges: 0,
};

export function stateStaffCategory(
  state: StaffCategoryState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetStaffCategoryAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetStaffCategoryAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetStaffCategoryAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
