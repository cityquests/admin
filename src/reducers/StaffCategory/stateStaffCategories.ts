import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetStaffCategoriesAction } from '../../actions';
import { StaffCategory } from '../../models';

export interface StaffCategoriesState {
  isFetching: boolean;
  staffCategories_list: StaffCategory[];
}

const initialState: StaffCategoriesState = {
  isFetching: true,
  staffCategories_list: [],
};

export function stateStaffCategories(
  state: StaffCategoriesState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetStaffCategoriesAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetStaffCategoriesAction.success):
      return {
        ...state,
        staffCategories_list: action.payload,
        isFetching: false,
      };

    case getType(fetchGetStaffCategoriesAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
