import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateStaffCategoryAction } from '../../actions';
import { StaffCategory } from '../../models';

export interface UpdateStaffCategoryState extends StaffCategory {
  isFetching: boolean;
}

const initialState: UpdateStaffCategoryState = {
  isFetching: true,
  type: '',
  displayName: '',
  privileges: 0,
};

export function updateStaffCategory(
  state: UpdateStaffCategoryState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateStaffCategoryAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateStaffCategoryAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateStaffCategoryAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
