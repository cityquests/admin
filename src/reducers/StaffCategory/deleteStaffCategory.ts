import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteStaffCategoryAction } from '../../actions';

export interface DeleteStaffCategoryState {
  isFetching: boolean;
}

const initialState: DeleteStaffCategoryState = {
  isFetching: true,
};

export function deleteStaffCategory(
  state: DeleteStaffCategoryState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteStaffCategoryAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteStaffCategoryAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteStaffCategoryAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
