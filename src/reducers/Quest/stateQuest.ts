import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestAction } from '../../actions';
import { Quest } from '../../models';

export interface QuestState extends Quest {
  isFetching: boolean;
}

const initialState: QuestState = {
  isFetching: true,
  questId: '',
  title: '',
  address: '',
  logo: '',
  img: '',
  cost: 0,
  region: '',
  sale: 0,
  type: '',
  description: '',
  shotDesc: '',
  map: '',
  ending: '',
};

export function stateQuest(
  state: QuestState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetQuestAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
