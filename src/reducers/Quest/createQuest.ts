import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostQuestAction } from '../../actions';
import { Quest } from '../../models';

export interface CreateQuestState extends Quest {
  isFetching: boolean;
}

const initialState: CreateQuestState = {
  isFetching: false,
  questId: '',
  title: '',
  address: '',
  logo: '',
  img: '',
  cost: 0,
  region: '',
  sale: 0,
  type: '',
  description: '',
  shotDesc: '',
  map: '',
  ending: '',
};

export function createQuest(
  state: CreateQuestState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostQuestAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostQuestAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostQuestAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
