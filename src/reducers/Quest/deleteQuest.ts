import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteQuestAction } from '../../actions';

export interface DeleteQuestState {
  isFetching: boolean;
}

const initialState: DeleteQuestState = {
  isFetching: true,
};

export function deleteQuest(
  state: DeleteQuestState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteQuestAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteQuestAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteQuestAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
