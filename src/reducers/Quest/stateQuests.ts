import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestsAction } from '../../actions';
import { Quest } from '../../models';

export interface QuestsState {
  isFetching: boolean;
  questsList: Quest[];
}

const initialState: QuestsState = {
  isFetching: true,
  questsList: [],
};

export function stateQuests(
  state: QuestsState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestsAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestsAction.success):
      return {
        ...state,
        questsList: action.payload,
        isFetching: false,
      };

    case getType(fetchGetQuestsAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
