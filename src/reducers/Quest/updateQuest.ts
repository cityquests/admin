import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateQuestAction } from '../../actions';
import { Quest } from '../../models';

export interface UpdateQuestState extends Quest {
  isFetching: boolean;
}

const initialState: UpdateQuestState = {
  isFetching: true,
  questId: '',
  title: '',
  address: '',
  logo: '',
  img: '',
  cost: 0,
  region: '',
  sale: 0,
  type: '',
  description: '',
  shotDesc: '',
  map: '',
  ending: '',
};

export function updateQuest(
  state: UpdateQuestState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateQuestAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateQuestAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateQuestAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
