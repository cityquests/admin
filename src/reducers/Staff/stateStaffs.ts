import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetStaffsAction } from '../../actions';
import { Staff } from '../../models';

export interface StaffsState {
  isFetching: boolean;
  staffs_list: Staff[];
}

const initialState: StaffsState = {
  isFetching: true,
  staffs_list: [],
};

export function stateStaffs(
  state: StaffsState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetStaffsAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetStaffsAction.success):
      return {
        ...state,
        staffs_list: action.payload,
        isFetching: false,
      };

    case getType(fetchGetStaffsAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
