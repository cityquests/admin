import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateStaffAction } from '../../actions';
import { Staff } from '../../models';

export interface UpdateStaffState extends Staff {
  isFetching: boolean;
}

const initialState: UpdateStaffState = {
  isFetching: true,
  name: '',
  email: '',
  password: '',
  category: '',
  lastLoging: '',
  phone: '',
};

export function updateStaff(
  state: UpdateStaffState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateStaffAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateStaffAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateStaffAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
