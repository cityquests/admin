import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteStaffAction } from '../../actions';

export interface DeleteStaffState {
  isFetching: boolean;
}

const initialState: DeleteStaffState = {
  isFetching: true,
};

export function deleteStaff(
  state: DeleteStaffState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteStaffAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteStaffAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteStaffAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
