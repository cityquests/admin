import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetStaffAction } from '../../actions';
import { Staff } from '../../models';

export interface StaffState extends Staff {
  isFetching: boolean;
}

const initialState: StaffState = {
  isFetching: true,
  name: '',
  email: '',
  password: '',
  category: '',
  lastLoging: '',
  phone: '',
};

export function stateStaff(
  state: StaffState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetStaffAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetStaffAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetStaffAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
