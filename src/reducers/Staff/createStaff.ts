import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostStaffAction } from '../../actions';
import { Staff } from '../../models';

export interface CreateStaffState extends Staff {
  isFetching: boolean;
}

const initialState: CreateStaffState = {
  isFetching: true,
  name: '',
  email: '',
  password: '',
  category: '',
  lastLoging: '',
  phone: '',
};

export function createStaff(
  state: CreateStaffState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostStaffAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostStaffAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostStaffAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
