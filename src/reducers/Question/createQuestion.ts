import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostQuestionAction } from '../../actions';
import { Question } from '../../models';

export interface CreateQuestionState extends Question {
  isFetching: boolean;
}

const initialState: CreateQuestionState = {
  isFetching: false,
  questionId: '',
  description: '',
  answer: '',
  fact: '',
  order: 0,
  img: '',
  map: '',
  type: '',
  questId: '',
};

export function createQuestion(
  state: CreateQuestionState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostQuestionAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostQuestionAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostQuestionAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
