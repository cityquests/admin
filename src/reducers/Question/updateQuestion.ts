import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateQuestionAction } from '../../actions';
import { Question } from '../../models';

export interface UpdateQuestionState extends Question {
  isFetching: boolean;
}

const initialState: UpdateQuestionState = {
  isFetching: true,
  questionId: '',
  description: '',
  answer: '',
  fact: '',
  order: 0,
  img: '',
  map: '',
  type: '',
  questId: '',
};

export function updateQuestion(
  state: UpdateQuestionState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateQuestionAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateQuestionAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateQuestionAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
