import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestionsAction } from '../../actions';
import { Question } from '../../models';

export interface QuestionsState {
  isFetching: boolean;
  questionsList: Question[];
}

const initialState: QuestionsState = {
  isFetching: true,
  questionsList: [],
};

export function stateQuestions(
  state: QuestionsState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestionsAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestionsAction.success):
      return {
        ...state,
        questionsList: action.payload.questons,
        isFetching: false,
      };

    case getType(fetchGetQuestionsAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
