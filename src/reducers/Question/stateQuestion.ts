import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetQuestionAction } from '../../actions';
import { Question } from '../../models';

export interface QuestionState extends Question {
  isFetching: boolean;
}

const initialState: QuestionState = {
  isFetching: true,
  questionId: '',
  description: '',
  answer: '',
  fact: '',
  order: 0,
  img: '',
  map: '',
  type: '',
  questId: '',
};

export function stateQuestion(
  state: QuestionState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetQuestionAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetQuestionAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetQuestionAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
