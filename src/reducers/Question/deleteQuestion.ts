import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteQuestionAction } from '../../actions';

export interface DeleteQuestionState {
  isFetching: boolean;
}

const initialState: DeleteQuestionState = {
  isFetching: true,
};

export function deleteQuestion(
  state: DeleteQuestionState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteQuestionAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteQuestionAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteQuestionAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
