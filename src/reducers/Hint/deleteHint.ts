import { getType, PayloadAction } from 'typesafe-actions';
import { fetchDeleteHintAction } from '../../actions';

export interface DeleteHintState {
  isFetching: boolean;
}

const initialState: DeleteHintState = {
  isFetching: true,
};

export function deleteHint(
  state: DeleteHintState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchDeleteHintAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchDeleteHintAction.success):
      return {
        ...state,
        isFetching: false,
      };

    case getType(fetchDeleteHintAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
