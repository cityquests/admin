import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetHintAction } from '../../actions';
import { Hint } from '../../models';

export interface HintState extends Hint {
  isFetching: boolean;
}

const initialState: HintState = {
  isFetching: true,
  hintId: '',
  description: '',
  img: '',
  order: 0,
  type: '',
  questionId: '',
};

export function stateHint(
  state: HintState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetHintAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetHintAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchGetHintAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
