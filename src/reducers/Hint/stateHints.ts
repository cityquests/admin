import { getType, PayloadAction } from 'typesafe-actions';
import { fetchGetHintsAction } from '../../actions';
import { Hint } from '../../models';

export interface HintsState {
  isFetching: boolean;
  hintsList: Hint[];
}

const initialState: HintsState = {
  isFetching: false,
  hintsList: [],
};

export function stateHints(
  state: HintsState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchGetHintsAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchGetHintsAction.success):
      return {
        ...state,
        hintsList: action.payload.hints,
        isFetching: false,
      };

    case getType(fetchGetHintsAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
