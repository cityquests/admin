import { getType, PayloadAction } from 'typesafe-actions';
import { fetchUpdateHintAction } from '../../actions';
import { Hint } from '../../models';

export interface UpdateHintState extends Hint {
  isFetching: boolean;
}

const initialState: UpdateHintState = {
  isFetching: true,
  hintId: '',
  description: '',
  img: '',
  order: 0,
  type: '',
  questionId: '',
};

export function updateHint(
  state: UpdateHintState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchUpdateHintAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchUpdateHintAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchUpdateHintAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
