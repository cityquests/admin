import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPostHintAction } from '../../actions';
import { Hint } from '../../models';

export interface CreateHintState extends Hint {
  isFetching: boolean;
}

const initialState: CreateHintState = {
  isFetching: false,
  hintId: '',
  description: '',
  img: '',
  order: 0,
  type: '',
  questionId: '',
};

export function createHint(
  state: CreateHintState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPostHintAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPostHintAction.success):
      return {
        ...state,
        ...action.payload,
        isFetching: false,
      };

    case getType(fetchPostHintAction.failure):
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}
