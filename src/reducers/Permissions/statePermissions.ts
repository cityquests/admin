import { getType, PayloadAction } from 'typesafe-actions';
import { fetchPermissionsAction } from '../../actions';
import { Permissions } from '../../models';

export interface PermissionsState extends Permissions {
  isFetching: boolean;
  isFetched: boolean;
}

const initialState: PermissionsState = {
  isFetching: false,
  isFetched: false,
  role: '',
  permissions: {},
};

export function statePermissions(
  state: PermissionsState = initialState,
  action: PayloadAction<string, any>,
) {
  switch (action.type) {
    case getType(fetchPermissionsAction.request):
      return {
        ...state,
        isFetching: true,
      };

    case getType(fetchPermissionsAction.success):
      return {
        ...state,
        role: action.payload.role,
        permissions: action.payload.permissions,
        isFetching: false,
        isFetched: true,
      };

    case getType(fetchPermissionsAction.failure):
      return {
        ...state,
        isFetching: false,
        isFetched: true,
      };

    default:
      return state;
  }
}
