import { call, put } from 'redux-saga/effects';
import {
  fetchGetImageAction,
  fetchDeleteImageAction,
  fetchGetImagesAction,
  fetchPostImageAction,
  fetchUpdateImageAction,
} from '../actions';
import * as imageServices from '../services/Image';
import history from '../history';

export function* requestGetImages(
  action: ReturnType<typeof fetchGetImagesAction.request>,
): Generator {
  try {
    const Images: any = yield call(
      imageServices.getImages,
      action.payload.areaId,
      action.payload.page,
    );
    yield put(fetchGetImagesAction.success(Images));
  } catch (e) {
    yield put(fetchGetImagesAction.failure(e));
  }
}

export function* requestGetImage(
  action: ReturnType<typeof fetchGetImageAction.request>,
): Generator {
  try {
    const Image: any = yield call(
      imageServices.getImage,
      action.payload.imageId,
    );
    yield put(fetchGetImageAction.success(Image));
  } catch (e) {
    yield put(fetchGetImageAction.failure(e));
  }
}

export function* requestDeleteImage(
  action: ReturnType<typeof fetchDeleteImageAction.request>,
): Generator {
  try {
    yield call(
      imageServices.deleteImage,
      action.payload.imageId,
    );
    const Images: any = yield call(
      imageServices.getImages,
      action.payload.areaId,
      1,
    );
    yield put(fetchGetImagesAction.success(Images));
    yield put(fetchDeleteImageAction.success());
  } catch (e) {
    yield put(fetchDeleteImageAction.failure(e));
  }
}

export function* requestPostImage(
  action: ReturnType<typeof fetchPostImageAction.request>,
): Generator {
  try {
    yield call(imageServices.postImage, action.payload);
    yield put(fetchPostImageAction.success());
    history.push('/images/list');
  } catch (e) {
    yield put(fetchPostImageAction.failure(e));
  }
}

export function* requestUpdateImage(
  action: ReturnType<typeof fetchUpdateImageAction.request>,
): Generator {
  try {
    const Image: any = yield call(
      imageServices.updateImage,
      action.payload,
    );
    yield put(fetchUpdateImageAction.success(Image));
  } catch (e) {
    yield put(fetchUpdateImageAction.failure(e));
  }
}
