import { takeEvery, all, takeLeading } from 'redux-saga/effects';
import {
  fetchGetUserAction,
  fetchGetUsersAction,
  fetchPostUserAction,
  fetchUpdateUserAction,
  fetchDeleteUserAction,
  fetchDeleteQuestAction,
  fetchGetQuestsAction,
  fetchGetQuestAction,
  fetchPostQuestAction,
  fetchUpdateQuestAction,
  fetchGetQuestionAction,
  fetchDeleteQuestionAction,
  fetchGetQuestionsAction,
  fetchPostQuestionAction,
  fetchUpdateQuestionAction,
  fetchDeleteHintAction,
  fetchGetHintAction,
  fetchGetHintsAction,
  fetchPostHintAction,
  fetchUpdateHintAction,
  fetchDeleteStaffAction,
  fetchGetStaffAction,
  fetchGetStaffsAction,
  fetchPostStaffAction,
  fetchUpdateStaffAction,
  fetchDeleteStaffCategoryAction,
  fetchGetStaffCategoryAction,
  fetchGetStaffCategoriesAction,
  fetchPostStaffCategoryAction,
  fetchUpdateStaffCategoryAction,
  fetchDeleteImageAction,
  fetchGetImageAction,
  fetchGetImagesAction,
  fetchPostImageAction,
  fetchUpdateImageAction,
  fetchDeleteQuestionTypeAction,
  fetchGetQuestionTypeAction,
  fetchGetQuestionTypesAction,
  fetchPostQuestionTypeAction,
  fetchUpdateQuestionTypeAction,
  fetchDeleteQuestionAreaAction,
  fetchGetQuestionAreaAction,
  fetchGetQuestionAreasAction,
  fetchPostQuestionAreaAction,
  fetchUpdateQuestionAreaAction,
  fetchLoginAction,
  fetchPermissionsAction,
} from '../actions';

import {
  requestPostUser,
  requestDeleteUser,
  requestGetUser,
  requestGetUsers,
  requestUpdateUser,
} from './user';

import {
  requestDeleteQuest,
  requestGetQuest,
  requestGetQuests,
  requestPostQuest,
  requestUpdateQuest,
} from './quest';

import {
  requestDeleteQuestion,
  requestGetQuestion,
  requestGetQuestions,
  requestPostQuestion,
  requestUpdateQuestion,
} from './question';

import {
  requestDeleteHint,
  requestGetHint,
  requestGetHints,
  requestPostHint,
  requestUpdateHint,
} from './hint';

import {
  requestDeleteStaff,
  requestGetStaff,
  requestGetStaffs,
  requestPostStaff,
  requestUpdateStaff,
} from './staff';

import {
  requestDeleteStaffCategory,
  requestGetStaffCategory,
  requestGetStaffCategories,
  requestPostStaffCategory,
  requestUpdateStaffCategory,
} from './staffCategory';

import {
  requestDeleteImage,
  requestGetImage,
  requestGetImages,
  requestPostImage,
  requestUpdateImage,
} from './image';

import {
  requestDeleteQuestionType,
  requestGetQuestionType,
  requestGetQuestionTypes,
  requestPostQuestionType,
  requestUpdateQuestionType,
} from './questionType';

import {
  requestDeleteQuestionArea,
  requestGetQuestionArea,
  requestGetQuestionAreas,
  requestPostQuestionArea,
  requestUpdateQuestionArea,
} from './questionArea';

import { requestLogin, requestPermissions } from './other';

export default function* rootSaga() {
  yield all([
    takeEvery(fetchGetUserAction.request, requestGetUser),
    takeEvery(fetchGetUsersAction.request, requestGetUsers),
    takeLeading(fetchDeleteUserAction.request, requestDeleteUser),
    takeLeading(fetchPostUserAction.request, requestPostUser),
    takeLeading(fetchUpdateUserAction.request, requestUpdateUser),

    takeEvery(fetchGetQuestAction.request, requestGetQuest),
    takeEvery(fetchGetQuestsAction.request, requestGetQuests),
    takeLeading(fetchDeleteQuestAction.request, requestDeleteQuest),
    takeLeading(fetchPostQuestAction.request, requestPostQuest),
    takeLeading(fetchUpdateQuestAction.request, requestUpdateQuest),

    takeEvery(fetchGetQuestionAction.request, requestGetQuestion),
    takeEvery(fetchGetQuestionsAction.request, requestGetQuestions),
    takeLeading(fetchDeleteQuestionAction.request, requestDeleteQuestion),
    takeLeading(fetchPostQuestionAction.request, requestPostQuestion),
    takeLeading(fetchUpdateQuestionAction.request, requestUpdateQuestion),

    takeEvery(fetchGetHintAction.request, requestGetHint),
    takeEvery(fetchGetHintsAction.request, requestGetHints),
    takeLeading(fetchDeleteHintAction.request, requestDeleteHint),
    takeLeading(fetchPostHintAction.request, requestPostHint),
    takeLeading(fetchUpdateHintAction.request, requestUpdateHint),

    takeEvery(fetchGetStaffAction.request, requestGetStaff),
    takeEvery(fetchGetStaffsAction.request, requestGetStaffs),
    takeLeading(fetchDeleteStaffAction.request, requestDeleteStaff),
    takeLeading(fetchPostStaffAction.request, requestPostStaff),
    takeLeading(fetchUpdateStaffAction.request, requestUpdateStaff),

    takeEvery(fetchGetStaffCategoryAction.request, requestGetStaffCategory),
    takeEvery(fetchGetStaffCategoriesAction.request, requestGetStaffCategories),
    takeLeading(
      fetchDeleteStaffCategoryAction.request,
      requestDeleteStaffCategory,
    ),
    takeLeading(fetchPostStaffCategoryAction.request, requestPostStaffCategory),
    takeLeading(
      fetchUpdateStaffCategoryAction.request,
      requestUpdateStaffCategory,
    ),

    takeEvery(fetchGetImageAction.request, requestGetImage),
    takeEvery(fetchGetImagesAction.request, requestGetImages),
    takeLeading(fetchDeleteImageAction.request, requestDeleteImage),
    takeLeading(fetchPostImageAction.request, requestPostImage),
    takeLeading(fetchUpdateImageAction.request, requestUpdateImage),

    takeEvery(fetchGetQuestionTypeAction.request, requestGetQuestionType),
    takeEvery(fetchGetQuestionTypesAction.request, requestGetQuestionTypes),
    takeLeading(
      fetchDeleteQuestionTypeAction.request,
      requestDeleteQuestionType,
    ),
    takeLeading(fetchPostQuestionTypeAction.request, requestPostQuestionType),
    takeLeading(
      fetchUpdateQuestionTypeAction.request,
      requestUpdateQuestionType,
    ),

    takeEvery(fetchGetQuestionAreaAction.request, requestGetQuestionArea),
    takeEvery(fetchGetQuestionAreasAction.request, requestGetQuestionAreas),
    takeLeading(
      fetchDeleteQuestionAreaAction.request,
      requestDeleteQuestionArea,
    ),
    takeLeading(fetchPostQuestionAreaAction.request, requestPostQuestionArea),
    takeLeading(
      fetchUpdateQuestionAreaAction.request,
      requestUpdateQuestionArea,
    ),

    takeLeading(fetchLoginAction.request, requestLogin),
    takeLeading(fetchPermissionsAction.request, requestPermissions),
  ]);
}
