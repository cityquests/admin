import { call, put } from 'redux-saga/effects';
import {
  fetchGetStaffCategoryAction,
  fetchDeleteStaffCategoryAction,
  fetchGetStaffCategoriesAction,
  fetchPostStaffCategoryAction,
  fetchUpdateStaffCategoryAction,
} from '../actions';
import * as staffCategoryServices from '../services/StaffCategory';
import history from '../history';

export function* requestGetStaffCategories(
  action: ReturnType<typeof fetchGetStaffCategoriesAction.request>,
): Generator {
  try {
    const StaffCategory: any = yield call(
      staffCategoryServices.getStaffCategories,
    );
    yield put(fetchGetStaffCategoriesAction.success(StaffCategory));
  } catch (e) {
    yield put(fetchGetStaffCategoriesAction.failure(e));
  }
}

export function* requestGetStaffCategory(
  action: ReturnType<typeof fetchGetStaffCategoryAction.request>,
): Generator {
  try {
    const StaffCategory: any = yield call(
      staffCategoryServices.getStaffCategory,
      action.payload.staffCategoryType,
    );
    yield put(fetchGetStaffCategoryAction.success(StaffCategory));
  } catch (e) {
    yield put(fetchGetStaffCategoryAction.failure(e));
  }
}

export function* requestDeleteStaffCategory(
  action: ReturnType<typeof fetchDeleteStaffCategoryAction.request>,
): Generator {
  try {
    yield call(
      staffCategoryServices.deleteStaffCategory,
      action.payload.staffCategoryType,
    );
    yield put(fetchDeleteStaffCategoryAction.success());
  } catch (e) {
    yield put(fetchDeleteStaffCategoryAction.failure(e));
  }
}

export function* requestPostStaffCategory(
  action: ReturnType<typeof fetchPostStaffCategoryAction.request>,
): Generator {
  try {
    yield call(staffCategoryServices.postStaffCategory, action.payload);
    history.push('/staff/list');
  } catch (e) {
    yield put(fetchPostStaffCategoryAction.failure(e));
  }
}

export function* requestUpdateStaffCategory(
  action: ReturnType<typeof fetchUpdateStaffCategoryAction.request>,
): Generator {
  try {
    const StaffCategory: any = yield call(
      staffCategoryServices.updateStaffCategory,
      action.payload,
    );
    yield put(fetchUpdateStaffCategoryAction.success(StaffCategory));
  } catch (e) {
    yield put(fetchUpdateStaffCategoryAction.failure(e));
  }
}
