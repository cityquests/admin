import { call, put } from 'redux-saga/effects';
import {
  fetchGetQuestAction,
  fetchDeleteQuestAction,
  fetchGetQuestsAction,
  fetchPostQuestAction,
  fetchUpdateQuestAction,
} from '../actions';
import * as questServices from '../services/Quest';
import history from '../history';

export function* requestGetQuests(
  action: ReturnType<typeof fetchGetQuestsAction.request>,
): Generator {
  try {
    const Quest: any = yield call(questServices.getQuests);
    yield put(fetchGetQuestsAction.success(Quest));
  } catch (e) {
    yield put(fetchGetQuestsAction.failure(e));
  }
}

export function* requestGetQuest(
  action: ReturnType<typeof fetchGetQuestAction.request>,
): Generator {
  try {
    const Quest: any = yield call(questServices.getQuest, action.payload.questId);
    yield put(fetchGetQuestAction.success(Quest));
  } catch (e) {
    yield put(fetchGetQuestAction.failure(e));
  }
}

export function* requestDeleteQuest(
  action: ReturnType<typeof fetchDeleteQuestAction.request>,
): Generator {
  try {
    yield call(questServices.deleteQuest, action.payload.questId);
    yield put(fetchDeleteQuestAction.success());
  } catch (e) {
    yield put(fetchDeleteQuestAction.failure(e));
  }
}

export function* requestPostQuest(
  action: ReturnType<typeof fetchPostQuestAction.request>,
): Generator {
  try {
    yield call(questServices.postQuest, action.payload);
    yield put(fetchPostQuestAction.success());
    history.push('/quests/list');
  } catch (e) {
    yield put(fetchPostQuestAction.failure(e));
  }
}

export function* requestUpdateQuest(
  action: ReturnType<typeof fetchUpdateQuestAction.request>,
): Generator {
  try {
    const Quest: any = yield call(questServices.updateQuest, action.payload);
    yield put(fetchUpdateQuestAction.success(Quest));
  } catch (e) {
    yield put(fetchUpdateQuestAction.failure(e));
  }
}
