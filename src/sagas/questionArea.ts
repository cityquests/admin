import { call, put } from 'redux-saga/effects';
import {
  fetchGetQuestionAreaAction,
  fetchDeleteQuestionAreaAction,
  fetchGetQuestionAreasAction,
  fetchPostQuestionAreaAction,
  fetchUpdateQuestionAreaAction,
} from '../actions';
import * as questionTypeServices from '../services/QuestionArea';
import history from '../history';

export function* requestGetQuestionAreas(
  action: ReturnType<typeof fetchGetQuestionAreasAction.request>,
): Generator {
  try {
    const QuestionArea: any = yield call(
      questionTypeServices.getQuestionAreas,
    );
    yield put(fetchGetQuestionAreasAction.success(QuestionArea));
  } catch (e) {
    yield put(fetchGetQuestionAreasAction.failure(e));
  }
}

export function* requestGetQuestionArea(
  action: ReturnType<typeof fetchGetQuestionAreaAction.request>,
): Generator {
  try {
    const QuestionArea: any = yield call(
      questionTypeServices.getQuestionArea,
      action.payload.areaId,
    );
    yield put(fetchGetQuestionAreaAction.success(QuestionArea));
  } catch (e) {
    yield put(fetchGetQuestionAreaAction.failure(e));
  }
}

export function* requestDeleteQuestionArea(
  action: ReturnType<typeof fetchDeleteQuestionAreaAction.request>,
): Generator {
  try {
    yield call(
      questionTypeServices.deleteQuestionArea,
      action.payload.areaId,
    );
    yield put(fetchDeleteQuestionAreaAction.success());
  } catch (e) {
    yield put(fetchDeleteQuestionAreaAction.failure(e));
  }
}

export function* requestPostQuestionArea(
  action: ReturnType<typeof fetchPostQuestionAreaAction.request>,
): Generator {
  try {
    yield call(questionTypeServices.postQuestionArea, action.payload);
    history.push('/questionarea/list');
  } catch (e) {
    yield put(fetchPostQuestionAreaAction.failure(e));
  }
}

export function* requestUpdateQuestionArea(
  action: ReturnType<typeof fetchUpdateQuestionAreaAction.request>,
): Generator {
  try {
    const QuestionArea: any = yield call(
      questionTypeServices.updateQuestionArea,
      action.payload,
    );
    yield put(fetchUpdateQuestionAreaAction.success(QuestionArea));
  } catch (e) {
    yield put(fetchUpdateQuestionAreaAction.failure(e));
  }
}
