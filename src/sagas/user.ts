import { call, put } from 'redux-saga/effects';
import {
  fetchGetUserAction,
  fetchDeleteUserAction,
  fetchGetUsersAction,
  fetchPostUserAction,
  fetchUpdateUserAction,
} from '../actions';
import * as userServices from '../services/User';
import history from '../history';

export function* requestGetUsers(
  action: ReturnType<typeof fetchGetUsersAction.request>,
): Generator {
  try {
    const User: any = yield call(userServices.getUsers);
    yield put(fetchGetUsersAction.success(User));
  } catch (e) {
    yield put(fetchGetUsersAction.failure(e));
  }
}

export function* requestGetUser(
  action: ReturnType<typeof fetchGetUserAction.request>,
): Generator {
  try {
    const User: any = yield call(
      userServices.getUser,
      action.payload.user_email,
    );
    yield put(fetchGetUserAction.success(User));
  } catch (e) {
    yield put(fetchGetUserAction.failure(e));
  }
}

export function* requestDeleteUser(
  action: ReturnType<typeof fetchDeleteUserAction.request>,
): Generator {
  try {
    yield call(userServices.deleteUser, action.payload.user_email);
    yield put(fetchDeleteUserAction.success());
  } catch (e) {
    yield put(fetchDeleteUserAction.failure(e));
  }
}

export function* requestPostUser(
  action: ReturnType<typeof fetchPostUserAction.request>,
): Generator {
  try {
    yield call(userServices.postUser, action.payload);
    history.push('/users/list');
  } catch (e) {
    yield put(fetchPostUserAction.failure(e));
  }
}

export function* requestUpdateUser(
  action: ReturnType<typeof fetchUpdateUserAction.request>,
): Generator {
  try {
    const User: any = yield call(userServices.updateUser, action.payload);
    yield put(fetchUpdateUserAction.success(User));
  } catch (e) {
    yield put(fetchUpdateUserAction.failure(e));
  }
}
