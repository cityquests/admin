import { call, put } from 'redux-saga/effects';
import {
  fetchGetStaffAction,
  fetchDeleteStaffAction,
  fetchGetStaffsAction,
  fetchPostStaffAction,
  fetchUpdateStaffAction,
} from '../actions';
import * as staffServices from '../services/Staff';
import history from '../history';

export function* requestGetStaffs(
  action: ReturnType<typeof fetchGetStaffsAction.request>,
): Generator {
  try {
    const Staff: any = yield call(staffServices.getStaffs);
    yield put(fetchGetStaffsAction.success(Staff));
  } catch (e) {
    yield put(fetchGetStaffsAction.failure(e));
  }
}

export function* requestGetStaff(
  action: ReturnType<typeof fetchGetStaffAction.request>,
): Generator {
  try {
    const Staff: any = yield call(
      staffServices.getStaff,
      action.payload.email,
    );
    yield put(fetchGetStaffAction.success(Staff));
  } catch (e) {
    yield put(fetchGetStaffAction.failure(e));
  }
}

export function* requestDeleteStaff(
  action: ReturnType<typeof fetchDeleteStaffAction.request>,
): Generator {
  try {
    yield call(staffServices.deleteStaff, action.payload.email);
    yield put(fetchDeleteStaffAction.success());
  } catch (e) {
    yield put(fetchDeleteStaffAction.failure(e));
  }
}

export function* requestPostStaff(
  action: ReturnType<typeof fetchPostStaffAction.request>,
): Generator {
  try {
    yield call(staffServices.postStaff, action.payload);
    history.push('/staff/list');
  } catch (e) {
    yield put(fetchPostStaffAction.failure(e));
  }
}

export function* requestUpdateStaff(
  action: ReturnType<typeof fetchUpdateStaffAction.request>,
): Generator {
  try {
    const Staff: any = yield call(staffServices.updateStaff, action.payload);
    yield put(fetchUpdateStaffAction.success(Staff));
  } catch (e) {
    yield put(fetchUpdateStaffAction.failure(e));
  }
}
