import { call, put } from 'redux-saga/effects';
import {
  fetchGetQuestionTypeAction,
  fetchDeleteQuestionTypeAction,
  fetchGetQuestionTypesAction,
  fetchPostQuestionTypeAction,
  fetchUpdateQuestionTypeAction,
} from '../actions';
import * as questionTypeServices from '../services/QuestionType';
import history from '../history';

export function* requestGetQuestionTypes(
  action: ReturnType<typeof fetchGetQuestionTypesAction.request>,
): Generator {
  try {
    const QuestionType: any = yield call(
      questionTypeServices.getQuestionTypes,
    );
    yield put(fetchGetQuestionTypesAction.success(QuestionType));
  } catch (e) {
    yield put(fetchGetQuestionTypesAction.failure(e));
  }
}

export function* requestGetQuestionType(
  action: ReturnType<typeof fetchGetQuestionTypeAction.request>,
): Generator {
  try {
    const QuestionType: any = yield call(
      questionTypeServices.getQuestionType,
      action.payload.typeId,
    );
    yield put(fetchGetQuestionTypeAction.success(QuestionType));
  } catch (e) {
    yield put(fetchGetQuestionTypeAction.failure(e));
  }
}

export function* requestDeleteQuestionType(
  action: ReturnType<typeof fetchDeleteQuestionTypeAction.request>,
): Generator {
  try {
    yield call(
      questionTypeServices.deleteQuestionType,
      action.payload.typeId,
    );
    yield put(fetchDeleteQuestionTypeAction.success());
  } catch (e) {
    yield put(fetchDeleteQuestionTypeAction.failure(e));
  }
}

export function* requestPostQuestionType(
  action: ReturnType<typeof fetchPostQuestionTypeAction.request>,
): Generator {
  try {
    yield call(questionTypeServices.postQuestionType, action.payload);
    history.push('/questiontype/list');
  } catch (e) {
    yield put(fetchPostQuestionTypeAction.failure(e));
  }
}

export function* requestUpdateQuestionType(
  action: ReturnType<typeof fetchUpdateQuestionTypeAction.request>,
): Generator {
  try {
    const QuestionType: any = yield call(
      questionTypeServices.updateQuestionType,
      action.payload,
    );
    yield put(fetchUpdateQuestionTypeAction.success(QuestionType));
  } catch (e) {
    yield put(fetchUpdateQuestionTypeAction.failure(e));
  }
}
