import { call, put } from 'redux-saga/effects';
import { fetchLoginAction, fetchPermissionsAction } from '../actions';
import * as otherServices from '../services/Other';
import conf from '../config';
import history from '../history';

export function* requestLogin(
  action: ReturnType<typeof fetchLoginAction.request>,
): Generator {
  try {
    const respn: any = yield call(otherServices.postLogin, action.payload);
    localStorage.setItem(conf.TOKEN, respn.token);
    if (history.location.pathname.includes('login')) {
      window.location = `${window.location.origin}/quests/list` as unknown as Location;
    } else {
      window.location.reload();
    }
  } catch (e) {
    yield put(fetchLoginAction.failure(e));
  }
}

export function* requestPermissions(
  action: ReturnType<typeof fetchPermissionsAction.request>,
): Generator {
  try {
    const permissions:any = yield call(otherServices.getPermissions);
    yield put(fetchPermissionsAction.success(permissions));
  } catch (e) {
    yield put(fetchPermissionsAction.failure(e));
  }
}
