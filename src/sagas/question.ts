import { call, put } from 'redux-saga/effects';
import {
  fetchGetQuestionAction,
  fetchDeleteQuestionAction,
  fetchGetQuestionsAction,
  fetchPostQuestionAction,
  fetchUpdateQuestionAction,
} from '../actions';
import * as questServices from '../services/Question';
import history from '../history';

export function* requestGetQuestions(
  action: ReturnType<typeof fetchGetQuestionsAction.request>,
): Generator {
  try {
    const Questions: any = yield call(questServices.getQuestions, action.payload.questId);
    yield put(fetchGetQuestionsAction.success(Questions));
  } catch (e) {
    yield put(fetchGetQuestionsAction.failure(e));
  }
}

export function* requestGetQuestion(
  action: ReturnType<typeof fetchGetQuestionAction.request>,
): Generator {
  try {
    const Question: any = yield call(
      questServices.getQuestion,
      action.payload.questionId,
    );
    yield put(fetchGetQuestionAction.success(Question));
  } catch (e) {
    yield put(fetchGetQuestionAction.failure(e));
  }
}

export function* requestDeleteQuestion(
  action: ReturnType<typeof fetchDeleteQuestionAction.request>,
): Generator {
  try {
    yield call(questServices.deleteQuestion, action.payload.questionId);
    yield put(fetchDeleteQuestionAction.success());
  } catch (e) {
    yield put(fetchDeleteQuestionAction.failure(e));
  }
}

export function* requestPostQuestion(
  action: ReturnType<typeof fetchPostQuestionAction.request>,
): Generator {
  try {
    yield call(questServices.postQuestion, action.payload);
    yield put(fetchPostQuestionAction.success());
    history.push('/questions/list');
  } catch (e) {
    yield put(fetchPostQuestionAction.failure(e));
  }
}

export function* requestUpdateQuestion(
  action: ReturnType<typeof fetchUpdateQuestionAction.request>,
): Generator {
  try {
    const Question: any = yield call(
      questServices.updateQuestion,
      action.payload,
    );
    yield put(fetchUpdateQuestionAction.success(Question));
  } catch (e) {
    yield put(fetchUpdateQuestionAction.failure(e));
  }
}
