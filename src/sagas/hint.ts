import { call, put } from 'redux-saga/effects';
import {
  fetchGetHintAction,
  fetchDeleteHintAction,
  fetchGetHintsAction,
  fetchPostHintAction,
  fetchUpdateHintAction,
} from '../actions';
import * as hintServices from '../services/Hint';
import history from '../history';

export function* requestGetHints(
  action: ReturnType<typeof fetchGetHintsAction.request>,
): Generator {
  try {
    const Hint: any = yield call(hintServices.getHints, action.payload.questionId);
    yield put(fetchGetHintsAction.success(Hint));
  } catch (e) {
    yield put(fetchGetHintsAction.failure(e));
  }
}

export function* requestGetHint(
  action: ReturnType<typeof fetchGetHintAction.request>,
): Generator {
  try {
    const Hint: any = yield call(hintServices.getHint, action.payload.hintId);
    yield put(fetchGetHintAction.success(Hint));
  } catch (e) {
    yield put(fetchGetHintAction.failure(e));
  }
}

export function* requestDeleteHint(
  action: ReturnType<typeof fetchDeleteHintAction.request>,
): Generator {
  try {
    yield call(hintServices.deleteHint, action.payload.hintId);
    yield put(fetchDeleteHintAction.success());
  } catch (e) {
    yield put(fetchDeleteHintAction.failure(e));
  }
}

export function* requestPostHint(
  action: ReturnType<typeof fetchPostHintAction.request>,
): Generator {
  try {
    yield call(hintServices.postHint, action.payload);
    yield put(fetchPostHintAction.success());
    history.push('/hints/list');
  } catch (e) {
    yield put(fetchPostHintAction.failure(e));
  }
}

export function* requestUpdateHint(
  action: ReturnType<typeof fetchUpdateHintAction.request>,
): Generator {
  try {
    const Hint: any = yield call(hintServices.updateHint, action.payload);
    yield put(fetchUpdateHintAction.success(Hint));
  } catch (e) {
    yield put(fetchUpdateHintAction.failure(e));
  }
}
