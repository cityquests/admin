import AppConfig from '../config';
import { Login, Permissions } from '../models';
import axios from './axios';

export async function postLogin(loginInfo: Login): Promise<Login> {
  const response = await axios.post<Login>(`${AppConfig.API_URL}/login`, {
    method: 'POST',
    ...loginInfo,
  });
  return response.data;
}

export async function getPermissions(): Promise<Permissions> {
  const response = await axios.get<Permissions>(`${AppConfig.API_URL}/permissions`);
  return response.data;
}
