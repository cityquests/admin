import AppConfig from '../config';
import { StaffCategory } from '../models';
import axios from './axios';

export async function getStaffCategories(): Promise<StaffCategory[]> {
  const response = await axios.get<StaffCategory[]>(
    `${AppConfig.API_URL}/staff_categories`,
  );
  return response.data;
}

export async function getStaffCategory(
  staffCategoryType: string,
): Promise<StaffCategory> {
  const response = await axios.get<StaffCategory>(
    `${AppConfig.API_URL}/staff_categories/${staffCategoryType}`,
  );
  return response.data;
}

export async function deleteStaffCategory(
  staffCategoryType: string,
): Promise<StaffCategory> {
  const response = await axios.delete<StaffCategory>(
    `${AppConfig.API_URL}/staff_categories/${staffCategoryType}`,
    {
      method: 'DELETE',
    },
  );
  return response.data;
}

export async function postStaffCategory(
  staffCategory: StaffCategory,
): Promise<StaffCategory> {
  const response = await axios.post<StaffCategory>(
    `${AppConfig.API_URL}/staff_categories`,
    {
      method: 'POST',
      ...staffCategory,
    },
  );
  return response.data;
}

export async function updateStaffCategory(
  staffCategory: StaffCategory,
): Promise<StaffCategory> {
  const response = await axios.put<StaffCategory>(
    `${AppConfig.API_URL}/staff_categories`,
    {
      method: 'PUT',
      ...staffCategory,
    },
  );
  return response.data;
}
