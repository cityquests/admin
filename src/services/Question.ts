import AppConfig from '../config';
import { Question } from '../models';
import axios from './axios';

export async function getQuestions(questId: string): Promise<Question[]> {
  const response = await axios.get<Question[]>(`${AppConfig.API_URL}/quest/${questId}/questions`);
  return response.data;
}

export async function getQuestion(questionId: string): Promise<Question> {
  const response = await axios.get<Question>(`${AppConfig.API_URL}/question/${questionId}`);
  return response.data;
}

export async function deleteQuestion(questionId: string): Promise<Question> {
  const response = await axios.delete<Question>(`${AppConfig.API_URL}/question/${questionId}`, {
    method: 'DELETE',
  });
  return response.data;
}

export async function postQuestion(question: Question): Promise<Question> {
  const formData = new FormData();
  for (let [key, value] of Object.entries(question)) {
    formData.append(key, value);
  }
  const response = await axios.post<Question>(`${AppConfig.API_URL}/question`, formData);
  return response.data;
}

export async function updateQuestion(question: Question): Promise<Question> {
  const response = await axios.put<Question>(`${AppConfig.API_URL}/question`, {
    method: 'PUT',
    ...question,
  });
  return response.data;
}
