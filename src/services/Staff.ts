import AppConfig from '../config';
import { Staff } from '../models';
import axios from './axios';

export async function getStaffs(): Promise<Staff[]> {
  const response = await axios.get<Staff[]>(`${AppConfig.API_URL}/staff`);
  return response.data;
}

export async function getStaff(email: string): Promise<Staff> {
  const response = await axios.get<Staff>(
    `${AppConfig.API_URL}/staff/${email}`,
  );
  return response.data;
}

export async function deleteStaff(email: string): Promise<Staff> {
  const response = await axios.delete<Staff>(
    `${AppConfig.API_URL}/staff/${email}`,
    {
      method: 'DELETE',
    },
  );
  return response.data;
}

export async function postStaff(staff: Staff): Promise<Staff> {
  const response = await axios.post<Staff>(`${AppConfig.API_URL}/staff`, {
    method: 'POST',
    ...staff,
  });
  return response.data;
}

export async function updateStaff(staff: Staff): Promise<Staff> {
  const response = await axios.put<Staff>(`${AppConfig.API_URL}/staff`, {
    method: 'PUT',
    ...staff,
  });
  return response.data;
}
