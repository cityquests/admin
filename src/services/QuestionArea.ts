import AppConfig from '../config';
import { QuestionArea } from '../models';
import axios from './axios';

export async function getQuestionAreas(): Promise<QuestionArea[]> {
  const response = await axios.get<QuestionArea[]>(`${AppConfig.API_URL}/question_area`);
  return response.data;
}

export async function getQuestionArea(areaId: string): Promise<QuestionArea> {
  const response = await axios.get<QuestionArea>(
    `${AppConfig.API_URL}/question_area/${areaId}`,
  );
  return response.data;
}

export async function deleteQuestionArea(areaId: string): Promise<QuestionArea> {
  const response = await axios.delete<QuestionArea>(
    `${AppConfig.API_URL}/question_area/${areaId}`,
    {
      method: 'DELETE',
    },
  );
  return response.data;
}

export async function postQuestionArea(question_area: QuestionArea): Promise<QuestionArea> {
  const response = await axios.post<QuestionArea>(`${AppConfig.API_URL}/question_area`, {
    method: 'POST',
    ...question_area,
  });
  return response.data;
}

export async function updateQuestionArea(question_area: QuestionArea): Promise<QuestionArea> {
  const response = await axios.put<QuestionArea>(`${AppConfig.API_URL}/question_area`, {
    method: 'PUT',
    ...question_area,
  });
  return response.data;
}
