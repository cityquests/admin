import AppConfig from '../config';
import { Image } from '../models';
import axios from './axios';

export async function getImages(areaId: number, page: number): Promise<Image[]> {
  const response = await axios.get<Image[]>(`${AppConfig.API_URL}/image/${areaId}/${page}`);
  return response.data;
}

export async function getImage(imageId: string): Promise<Image> {
  const response = await axios.get<Image>(
    `${AppConfig.API_URL}/image/${imageId}`,
  );
  return response.data;
}

export async function deleteImage(imageId: string): Promise<Image> {
  const response = await axios.delete<Image>(
    `${AppConfig.API_URL}/image/${imageId}`,
    {
      method: 'DELETE',
    },
  );
  return response.data;
}

export async function postImage(image: Image): Promise<Image> {
  const formData = new FormData();
  for (let [key, value] of Object.entries(image)) {
    formData.append(key, value);
  }
  const response = await axios.post(`${AppConfig.API_URL}/image`, formData, {});
  return response.data;
}

export async function updateImage(image: Image): Promise<Image> {
  const response = await axios.put<Image>(`${AppConfig.API_URL}/image`, {
    method: 'PUT',
    ...image,
  });
  return response.data;
}
