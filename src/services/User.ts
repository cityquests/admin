import AppConfig from '../config';
import { User } from '../models';
import axios from './axios';

export async function getUsers(): Promise<User[]> {
  const response = await axios.get<User[]>(`${AppConfig.API_URL}/user`);
  return response.data;
}

export async function getUser(user_email: string): Promise<User> {
  const response = await axios.get<User>(
    `${AppConfig.API_URL}/user/${user_email}`,
  );
  return response.data;
}

export async function deleteUser(user_email: string): Promise<User> {
  const response = await axios.delete<User>(
    `${AppConfig.API_URL}/user/${user_email}`,
    {
      method: 'DELETE',
    },
  );
  return response.data;
}

export async function postUser(user: User): Promise<User> {
  const response = await axios.post<User>(`${AppConfig.API_URL}/user`, {
    method: 'POST',
    ...user,
  });
  return response.data;
}

export async function updateUser(user: User): Promise<User> {
  const response = await axios.put<User>(`${AppConfig.API_URL}/user`, {
    method: 'PUT',
    ...user,
  });
  return response.data;
}
