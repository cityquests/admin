import AppConfig from '../config';
import { QuestionType } from '../models';
import axios from './axios';

export async function getQuestionTypes(): Promise<QuestionType[]> {
  const response = await axios.get<QuestionType[]>(`${AppConfig.API_URL}/question_type`);
  return response.data;
}

export async function getQuestionType(typeId: string): Promise<QuestionType> {
  const response = await axios.get<QuestionType>(
    `${AppConfig.API_URL}/question_type/${typeId}`,
  );
  return response.data;
}

export async function deleteQuestionType(typeId: string): Promise<QuestionType> {
  const response = await axios.delete<QuestionType>(
    `${AppConfig.API_URL}/question_type/${typeId}`,
    {
      method: 'DELETE',
    },
  );
  return response.data;
}

export async function postQuestionType(question_type: QuestionType): Promise<QuestionType> {
  const response = await axios.post<QuestionType>(`${AppConfig.API_URL}/question_type`, {
    method: 'POST',
    ...question_type,
  });
  return response.data;
}

export async function updateQuestionType(question_type: QuestionType): Promise<QuestionType> {
  const response = await axios.put<QuestionType>(`${AppConfig.API_URL}/question_type`, {
    method: 'PUT',
    ...question_type,
  });
  return response.data;
}
