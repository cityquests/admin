import AppConfig from '../config';
import { Hint } from '../models';
import axios from './axios';

export async function getHints(questionId: string): Promise<Hint[]> {
  const response = await axios.get<Hint[]>(`${AppConfig.API_URL}/question/${questionId}/hints`);
  return response.data;
}

export async function getHint(hintId: string): Promise<Hint> {
  const response = await axios.get<Hint>(`${AppConfig.API_URL}/hint/${hintId}`);
  return response.data;
}

export async function deleteHint(hintId: string): Promise<Hint> {
  const response = await axios.delete<Hint>(
    `${AppConfig.API_URL}/hint/${hintId}`,
    {
      method: 'DELETE',
    },
  );
  return response.data;
}

export async function postHint(hint: Hint): Promise<Hint> {
  const formData = new FormData();
  for (let [key, value] of Object.entries(hint)) {
    formData.append(key, value);
  }
  const response = await axios.post<Hint>(`${AppConfig.API_URL}/hint`, formData);
  return response.data;
}

export async function updateHint(hint: Hint): Promise<Hint> {
  const response = await axios.put<Hint>(`${AppConfig.API_URL}/hint`, {
    method: 'PUT',
    ...hint,
  });
  return response.data;
}
