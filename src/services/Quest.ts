import AppConfig from '../config';
import { Quest } from '../models';
import axios from './axios';

export async function getQuests(): Promise<Quest[]> {
  const response = await axios.get<Quest[]>(`${AppConfig.API_URL}/quest`);
  return response.data;
}

export async function getQuest(questId: string): Promise<Quest> {
  const response = await axios.get<Quest>(`${AppConfig.API_URL}/quest/${questId}`);
  return response.data;
}

export async function deleteQuest(questId: string): Promise<Quest> {
  const response = await axios.delete<Quest>(`${AppConfig.API_URL}/quest/${questId}`, {
    method: 'DELETE',
  });
  return response.data;
}

export async function postQuest(quest: Quest): Promise<Quest> {
  const formData = new FormData();
  for (let [key, value] of Object.entries(quest)) {
    formData.append(key, value);
  }
  const response = await axios.post<Quest>(`${AppConfig.API_URL}/quest`, formData);
  return response.data;
}

export async function updateQuest(quest: Quest): Promise<Quest> {
  const response = await axios.put<Quest>(`${AppConfig.API_URL}/quest`, {
    method: 'PUT',
    ...quest,
  });
  return response.data;
}
