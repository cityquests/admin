import { createAsyncAction } from 'typesafe-actions';
import {
  User,
  Quest,
  Question,
  Hint,
  Staff,
  StaffCategory,
  Image,
  QuestionArea,
  QuestionType,
  Login,
  Permissions,
} from '../models';

const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILURE = 'FAILURE';
const CANCEL = 'CANCEL';

function createRequestTypes(base: string) {
  return [REQUEST, SUCCESS, FAILURE, CANCEL].reduce(
    (acc: any, type: string) => {
      acc[type] = `${base}_${type}`;
      return acc;
    },
    {},
  );
}

const USERS_GET = createRequestTypes('USERS_GET');
const USER_UPDATE = createRequestTypes('USER_UPDATE');
const USER_POST = createRequestTypes('USER_POST');
const USER_DELETE = createRequestTypes('USER_DELETE');
const USER_GET = createRequestTypes('USER_GET');
const QUESTS_GET = createRequestTypes('QUESTS_GET');
const QUEST_UPDATE = createRequestTypes('QUEST_UPDATE');
const QUEST_POST = createRequestTypes('QUEST_POST');
const QUEST_DELETE = createRequestTypes('QUEST_DELETE');
const QUEST_GET = createRequestTypes('QUEST_GET');
const QUESTIONS_GET = createRequestTypes('QUESTIONS_GET');
const QUESTION_UPDATE = createRequestTypes('QUESTION_UPDATE');
const QUESTION_POST = createRequestTypes('QUESTION_POST');
const QUESTION_DELETE = createRequestTypes('QUESTION_DELETE');
const QUESTION_GET = createRequestTypes('QUESTION_GET');
const HINTS_GET = createRequestTypes('HINTS_GET');
const HINT_UPDATE = createRequestTypes('HINT_UPDATE');
const HINT_POST = createRequestTypes('HINT_POST');
const HINT_DELETE = createRequestTypes('HINT_DELETE');
const HINT_GET = createRequestTypes('HINT_GET');
const STAFF_CATEGORIES_GET = createRequestTypes('STAFF_CATEGORIES_GET');
const STAFF_CATEGORY_UPDATE = createRequestTypes('STAFF_CATEGORY_UPDATE');
const STAFF_CATEGORY_POST = createRequestTypes('STAFF_CATEGORY_POST');
const STAFF_CATEGORY_DELETE = createRequestTypes('STAFF_CATEGORY_DELETE');
const STAFF_CATEGORY_GET = createRequestTypes('STAFF_CATEGORY_GET');
const STAFFS_GET = createRequestTypes('STAFFS_GET');
const STAFF_UPDATE = createRequestTypes('STAFF_UPDATE');
const STAFF_POST = createRequestTypes('STAFF_POST');
const STAFF_DELETE = createRequestTypes('STAFF_DELETE');
const STAFF_GET = createRequestTypes('STAFF_GET');
const IMAGES_GET = createRequestTypes('IMAGES_GET');
const IMAGE_GET = createRequestTypes('IMAGE_GET');
const IMAGE_UPDATE = createRequestTypes('IMAGE_UPDATE');
const IMAGE_POST = createRequestTypes('IMAGE_POST');
const IMAGE_DELETE = createRequestTypes('IMAGE_DELETE');
const QUESTION_TYPES_GET = createRequestTypes('QUESTION_TYPES_GET');
const QUESTION_TYPE_GET = createRequestTypes('QUESTION_TYPE_GET');
const QUESTION_TYPE_UPDATE = createRequestTypes('QUESTION_TYPE_UPDATE');
const QUESTION_TYPE_POST = createRequestTypes('QUESTION_TYPE_POST');
const QUESTION_TYPE_DELETE = createRequestTypes('QUESTION_TYPE_DELETE');
const QUESTION_AREAS_GET = createRequestTypes('QUESTION_AREAS_GET');
const QUESTION_AREA_GET = createRequestTypes('QUESTION_AREA_GET');
const QUESTION_AREA_UPDATE = createRequestTypes('QUESTION_AREA_UPDATE');
const QUESTION_AREA_POST = createRequestTypes('QUESTION_AREA_POST');
const QUESTION_AREA_DELETE = createRequestTypes('QUESTION_AREA_DELETE');
const LOGIN_POST = createRequestTypes('LOGIN_POST');
const PERMISSIONS_GET = createRequestTypes('PERMISSIONS_GET');

// USER
export const fetchGetUsersAction = createAsyncAction(
  USERS_GET[REQUEST],
  USERS_GET[SUCCESS],
  USERS_GET[FAILURE],
)<undefined, User[], Error>();

export const fetchGetUserAction = createAsyncAction(
  USER_GET[REQUEST],
  USER_GET[SUCCESS],
  USER_GET[FAILURE],
)<{ user_email: string }, User, Error>();

export const fetchDeleteUserAction = createAsyncAction(
  USER_DELETE[REQUEST],
  USER_DELETE[SUCCESS],
  USER_DELETE[FAILURE],
)<{ user_email: string }, undefined, Error>();

export const fetchPostUserAction = createAsyncAction(
  USER_POST[REQUEST],
  USER_POST[SUCCESS],
  USER_POST[FAILURE],
)<User, undefined, Error>();

export const fetchUpdateUserAction = createAsyncAction(
  USER_UPDATE[REQUEST],
  USER_UPDATE[SUCCESS],
  USER_UPDATE[FAILURE],
)<User, User, Error>();

// QUEST
export const fetchGetQuestsAction = createAsyncAction(
  QUESTS_GET[REQUEST],
  QUESTS_GET[SUCCESS],
  QUESTS_GET[FAILURE],
)<undefined, Quest[], Error>();

export const fetchGetQuestAction = createAsyncAction(
  QUEST_GET[REQUEST],
  QUEST_GET[SUCCESS],
  QUEST_GET[FAILURE],
)<{ questId: string }, Quest, Error>();

export const fetchDeleteQuestAction = createAsyncAction(
  QUEST_DELETE[REQUEST],
  QUEST_DELETE[SUCCESS],
  QUEST_DELETE[FAILURE],
)<{ questId: string }, undefined, Error>();

export const fetchPostQuestAction = createAsyncAction(
  QUEST_POST[REQUEST],
  QUEST_POST[SUCCESS],
  QUEST_POST[FAILURE],
)<Quest, undefined, Error>();

export const fetchUpdateQuestAction = createAsyncAction(
  QUEST_UPDATE[REQUEST],
  QUEST_UPDATE[SUCCESS],
  QUEST_UPDATE[FAILURE],
)<Quest, Quest, Error>();

// QUESTION
export const fetchGetQuestionsAction = createAsyncAction(
  QUESTIONS_GET[REQUEST],
  QUESTIONS_GET[SUCCESS],
  QUESTIONS_GET[FAILURE],
)<{ questId: string }, Question[], Error>();

export const fetchGetQuestionAction = createAsyncAction(
  QUESTION_GET[REQUEST],
  QUESTION_GET[SUCCESS],
  QUESTION_GET[FAILURE],
)<{ questionId: string }, Question, Error>();

export const fetchDeleteQuestionAction = createAsyncAction(
  QUESTION_DELETE[REQUEST],
  QUESTION_DELETE[SUCCESS],
  QUESTION_DELETE[FAILURE],
)<{ questionId: string }, undefined, Error>();

export const fetchPostQuestionAction = createAsyncAction(
  QUESTION_POST[REQUEST],
  QUESTION_POST[SUCCESS],
  QUESTION_POST[FAILURE],
)<Question, undefined, Error>();

export const fetchUpdateQuestionAction = createAsyncAction(
  QUESTION_UPDATE[REQUEST],
  QUESTION_UPDATE[SUCCESS],
  QUESTION_UPDATE[FAILURE],
)<Question, Question, Error>();

// HINT
export const fetchGetHintsAction = createAsyncAction(
  HINTS_GET[REQUEST],
  HINTS_GET[SUCCESS],
  HINTS_GET[FAILURE],
)<{ questionId: string }, Hint[], Error>();

export const fetchGetHintAction = createAsyncAction(
  HINT_GET[REQUEST],
  HINT_GET[SUCCESS],
  HINT_GET[FAILURE],
)<{ hintId: string }, Hint, Error>();

export const fetchDeleteHintAction = createAsyncAction(
  HINT_DELETE[REQUEST],
  HINT_DELETE[SUCCESS],
  HINT_DELETE[FAILURE],
)<{ hintId: string }, undefined, Error>();

export const fetchPostHintAction = createAsyncAction(
  HINT_POST[REQUEST],
  HINT_POST[SUCCESS],
  HINT_POST[FAILURE],
)<Hint, undefined, Error>();

export const fetchUpdateHintAction = createAsyncAction(
  HINT_UPDATE[REQUEST],
  HINT_UPDATE[SUCCESS],
  HINT_UPDATE[FAILURE],
)<Hint, Hint, Error>();

// StaffCategory
export const fetchGetStaffCategoriesAction = createAsyncAction(
  STAFF_CATEGORIES_GET[REQUEST],
  STAFF_CATEGORIES_GET[SUCCESS],
  STAFF_CATEGORIES_GET[FAILURE],
)<undefined, StaffCategory[], Error>();

export const fetchGetStaffCategoryAction = createAsyncAction(
  STAFF_CATEGORY_GET[REQUEST],
  STAFF_CATEGORY_GET[SUCCESS],
  STAFF_CATEGORY_GET[FAILURE],
)<{ staffCategoryType: string }, StaffCategory, Error>();

export const fetchDeleteStaffCategoryAction = createAsyncAction(
  STAFF_CATEGORY_DELETE[REQUEST],
  STAFF_CATEGORY_DELETE[SUCCESS],
  STAFF_CATEGORY_DELETE[FAILURE],
)<{ staffCategoryType: string }, undefined, Error>();

export const fetchPostStaffCategoryAction = createAsyncAction(
  STAFF_CATEGORY_POST[REQUEST],
  STAFF_CATEGORY_POST[SUCCESS],
  STAFF_CATEGORY_POST[FAILURE],
)<StaffCategory, undefined, Error>();

export const fetchUpdateStaffCategoryAction = createAsyncAction(
  STAFF_CATEGORY_UPDATE[REQUEST],
  STAFF_CATEGORY_UPDATE[SUCCESS],
  STAFF_CATEGORY_UPDATE[FAILURE],
)<StaffCategory, StaffCategory, Error>();

// Staff
export const fetchGetStaffsAction = createAsyncAction(
  STAFFS_GET[REQUEST],
  STAFFS_GET[SUCCESS],
  STAFFS_GET[FAILURE],
)<undefined, Staff[], Error>();

export const fetchGetStaffAction = createAsyncAction(
  STAFF_GET[REQUEST],
  STAFF_GET[SUCCESS],
  STAFF_GET[FAILURE],
)<{ email: string }, Staff, Error>();

export const fetchDeleteStaffAction = createAsyncAction(
  STAFF_DELETE[REQUEST],
  STAFF_DELETE[SUCCESS],
  STAFF_DELETE[FAILURE],
)<{ email: string }, undefined, Error>();

export const fetchPostStaffAction = createAsyncAction(
  STAFF_POST[REQUEST],
  STAFF_POST[SUCCESS],
  STAFF_POST[FAILURE],
)<Staff, undefined, Error>();

export const fetchUpdateStaffAction = createAsyncAction(
  STAFF_UPDATE[REQUEST],
  STAFF_UPDATE[SUCCESS],
  STAFF_UPDATE[FAILURE],
)<Staff, Staff, Error>();

// image
export const fetchGetImagesAction = createAsyncAction(
  IMAGES_GET[REQUEST],
  IMAGES_GET[SUCCESS],
  IMAGES_GET[FAILURE],
)<{ areaId: number, page: number }, Image[], Error>();

export const fetchGetImageAction = createAsyncAction(
  IMAGE_GET[REQUEST],
  IMAGE_GET[SUCCESS],
  IMAGE_GET[FAILURE],
)<{ imageId: string }, Image, Error>();

export const fetchDeleteImageAction = createAsyncAction(
  IMAGE_DELETE[REQUEST],
  IMAGE_DELETE[SUCCESS],
  IMAGE_DELETE[FAILURE],
)<{ imageId: string, areaId: number }, undefined, Error>();

export const fetchPostImageAction = createAsyncAction(
  IMAGE_POST[REQUEST],
  IMAGE_POST[SUCCESS],
  IMAGE_POST[FAILURE],
)<Image, undefined, Error>();

export const fetchUpdateImageAction = createAsyncAction(
  IMAGE_UPDATE[REQUEST],
  IMAGE_UPDATE[SUCCESS],
  IMAGE_UPDATE[FAILURE],
)<Image, Image, Error>();

// question area
export const fetchGetQuestionAreasAction = createAsyncAction(
  QUESTION_AREAS_GET[REQUEST],
  QUESTION_AREAS_GET[SUCCESS],
  QUESTION_AREAS_GET[FAILURE],
)<undefined, QuestionArea[], Error>();

export const fetchGetQuestionAreaAction = createAsyncAction(
  QUESTION_AREA_GET[REQUEST],
  QUESTION_AREA_GET[SUCCESS],
  QUESTION_AREA_GET[FAILURE],
)<{ areaId: string }, QuestionArea, Error>();

export const fetchDeleteQuestionAreaAction = createAsyncAction(
  QUESTION_AREA_DELETE[REQUEST],
  QUESTION_AREA_DELETE[SUCCESS],
  QUESTION_AREA_DELETE[FAILURE],
)<{ areaId: string }, undefined, Error>();

export const fetchPostQuestionAreaAction = createAsyncAction(
  QUESTION_AREA_POST[REQUEST],
  QUESTION_AREA_POST[SUCCESS],
  QUESTION_AREA_POST[FAILURE],
)<QuestionArea, undefined, Error>();

export const fetchUpdateQuestionAreaAction = createAsyncAction(
  QUESTION_AREA_UPDATE[REQUEST],
  QUESTION_AREA_UPDATE[SUCCESS],
  QUESTION_AREA_UPDATE[FAILURE],
)<QuestionArea, QuestionArea, Error>();

// question type
export const fetchGetQuestionTypesAction = createAsyncAction(
  QUESTION_TYPES_GET[REQUEST],
  QUESTION_TYPES_GET[SUCCESS],
  QUESTION_TYPES_GET[FAILURE],
)<undefined, QuestionType[], Error>();

export const fetchGetQuestionTypeAction = createAsyncAction(
  QUESTION_TYPE_GET[REQUEST],
  QUESTION_TYPE_GET[SUCCESS],
  QUESTION_TYPE_GET[FAILURE],
)<{ typeId: string }, QuestionType, Error>();

export const fetchDeleteQuestionTypeAction = createAsyncAction(
  QUESTION_TYPE_DELETE[REQUEST],
  QUESTION_TYPE_DELETE[SUCCESS],
  QUESTION_TYPE_DELETE[FAILURE],
)<{ typeId: string }, undefined, Error>();

export const fetchPostQuestionTypeAction = createAsyncAction(
  QUESTION_TYPE_POST[REQUEST],
  QUESTION_TYPE_POST[SUCCESS],
  QUESTION_TYPE_POST[FAILURE],
)<QuestionType, undefined, Error>();

export const fetchUpdateQuestionTypeAction = createAsyncAction(
  QUESTION_TYPE_UPDATE[REQUEST],
  QUESTION_TYPE_UPDATE[SUCCESS],
  QUESTION_TYPE_UPDATE[FAILURE],
)<QuestionType, QuestionType, Error>();

// Admin login
export const fetchLoginAction = createAsyncAction(
  LOGIN_POST[REQUEST],
  LOGIN_POST[SUCCESS],
  LOGIN_POST[FAILURE],
)<Login, { token: string }, Error>();

// get permisssions
export const fetchPermissionsAction = createAsyncAction(
  PERMISSIONS_GET[REQUEST],
  PERMISSIONS_GET[SUCCESS],
  PERMISSIONS_GET[FAILURE],
)<undefined, Permissions, Error>();
