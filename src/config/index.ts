export interface AppConfig {
  API_URL: string;
  GOOGLE_MAP_API: string;
  IMAGES_URL: string;
  TOKEN: string;
}

let AppConfig: () => AppConfig;

if (process.env.REACT_APP_ENV === 'local') {
  AppConfig = require('./config.local').default;
} else if (process.env.REACT_APP_ENV === 'dev') {
  AppConfig = require('./config.dev').default;
} else {
  AppConfig = require('./config.stg').default;
}

export default AppConfig();
