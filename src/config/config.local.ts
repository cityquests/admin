export default function() {
  return {
    API_URL: 'http://localhost:8000/api',
    GOOGLE_MAP_API: 'AIzaSyDbRlK3t2HlNG53KvlN9rrNAd8bhcYel5I',
    IMAGES_URL: 'https://quests-bucket.s3-ap-northeast-1.amazonaws.com/',
    TOKEN: 'AUTH_TKN',
  };
}
