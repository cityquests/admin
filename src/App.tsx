import React from 'react';
import { Switch, Route, Router, Redirect } from 'react-router-dom';
import history from './history';
import './App.css';
import LeftSideBar from './components/LeftSideBar';
import Header from './containers/Header';
import PageWrapper from './containers/PageWrapper';
import LoginPage from './components/LoginPage';
import conf from './config';

const App: React.FC = () => {
  const [drawerIsOpen, setDrawerIsOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setDrawerIsOpen(true);
  };

  const handleDrawerClose = () => {
    setDrawerIsOpen(false);
  };

  if (!localStorage.getItem(conf.TOKEN)) {
    return <LoginPage />;
  }

  return (
    <div className="root">
      <Router history={history}>
        <Header
          drawerIsOpen={drawerIsOpen}
          handleDrawerOpen={handleDrawerOpen} />
        <LeftSideBar
          drawerIsOpen={drawerIsOpen}
          handleDrawerClose={handleDrawerClose} />
        <Switch>
          <Redirect exact={true} from="/" to="/quests/list/" />
          <Route exact={true} path="/:page_type/:page_function" component={PageWrapper} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
