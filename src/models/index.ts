export interface Hint {
  hintId: string;
  description: string;
  img: string;
  order: number;
  type: string;
  questionId: string;
}

export interface Question {
  questionId: string;
  description: string;
  answer: string;
  fact: string;
  order: number;
  img: string;
  map: string;
  type: string;
  questId: string;
}

export interface Quest {
  questId: string;
  title: string;
  address: string;
  logo: string;
  img: string;
  cost: number;
  region: string;
  sale: number;
  type: string;
  description: string;
  shotDesc: string;
  map: string;
  ending: string;
}

export interface StaffCategory {
  type: string;
  displayName: string;
  privileges: number;
}

export interface Staff {
  name: string;
  email: string;
  password: string;
  category: string;
  lastLoging: string;
  phone: string;
}

export interface User {
  fullname: string;
  description: string;
  email: string;
  password: string;
  phone: string;
  createdAt: string;
  lastLogin: string;
  nickname: string;
  birthday: string;
}

export interface Game {
  gameId: string;
  userId: string;
  questId: string;
  questionId: string;
  password: string;
  status: string;
  score: number;
  hints_used: number;
  last_login: Date;
  finished_at: Date;
}

export interface QuestionType {
  typeId: number;
  name: string;
}

export interface QuestionArea {
  areaId: number;
  name: string;
}

export interface Image {
  imageId: string;
  _id: string;
  url: string;
  areaId: number;
  typeId: number;
  location: number[];
  name: string;
}

export interface PaginatedImages {
  imagesList: Image[];
  currentPage: number;
  pages: number;
  numOfResults: number;
}

export interface Login {
  email: string;
  password: string;
  remember: boolean;
}

export interface Permissions {
  role: string;
  permissions: {
    [key: string]: string;
  };
}