import React from 'react';
import './inputGroup.css';
import TextField from '@material-ui/core/TextField';

export default function FileGroup(displayName: string,
  name: string, errorMsg: any,
  register: any, isRequired: boolean = true, other: any = {}) {
  return (
    <TextField
      variant="outlined"
      margin="normal"
      required={isRequired}
      fullWidth
      type='file'
      InputLabelProps={{
        shrink: true,
      }}
      label={displayName}
      name={name}
      error={errorMsg ? true : false}
      helperText={errorMsg}
      {...other}
      inputRef={register({ required: ' is required' })}
    />
  );
}
