import React from 'react';
import { useDispatch } from 'react-redux';
import {
  fetchPostQuestionTypeAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { QuestionType } from '../../models';
import { InputGroup } from '../Form';
import Button from '@material-ui/core/Button';

const QuestionTypesCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<QuestionType>();
  const onSubmit = (data: QuestionType) => {
    dispatch(fetchPostQuestionTypeAction.request(data));
  };
  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Type Id', 'typeId',
          errors.typeId && errors.typeId.message, 'text', register)}
        {InputGroup('Name', 'name',
          errors.name && errors.name.message, 'text', register)}
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">Create</Button>
      </form>
    </div>
  );
};

export default QuestionTypesCreate;