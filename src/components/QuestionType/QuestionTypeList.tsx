import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import { fetchGetQuestionTypesAction } from '../../actions';
import TableSortPagination from '../TableSortPagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const headCells = [
  { id: 'typeId', align: 'left', label: 'Type Id' },
  { id: 'name', align: 'left', label: 'Name' },
];

const StaffCategoryList = () => {
  const dispatch = useDispatch();
  const stateQuestionTypes = useSelector((state: State) => state.stateQuestionTypes);
  useEffect(() => {
    dispatch(fetchGetQuestionTypesAction.request());
  }, [dispatch]);

  return (
    <TableSortPagination
      headCells={headCells}
      bodyRows={stateQuestionTypes.question_type_list}
      defaultSort={'name'}
      getBodyRow={(questionType: any) => (
        <TableRow key={questionType.name}>
          <TableCell scope="row">{questionType.typeId}</TableCell>
          <TableCell align="left">{questionType.name}</TableCell>
        </TableRow>
      )}
    />
  );
};

export default StaffCategoryList;