import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchPostQuestAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { State } from '../../reducers';
import { Quest } from '../../models';
import { InputGroup, FileGroup } from '../Form';
import { Button, CircularProgress } from '@material-ui/core';

const QuestsCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<Quest>();
  const stateCreateQuest = useSelector((state: State) => state.createQuest);
  const onSubmit = (data: Quest) => {
    const fd = new FormData();
    fd.set('a', data.img[0]);
    const fd2 = new FormData();
    fd2.set('a', data.logo[0]);
    dispatch(fetchPostQuestAction.request({
      ...data,
      // @ts-ignore
      img: fd.get('a'),
      // @ts-ignore
      logo: fd2.get('a'),
    }));
  };

  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Title', 'title', errors.title && errors.title.message, 'text', register)}
        {InputGroup('Address', 'address', errors.address && errors.address.message, 'text', register)}
        {FileGroup('Logo', 'logo', errors.logo && errors.logo.message, register)}
        {FileGroup('Img', 'img', errors.img && errors.img.message, register)}
        {InputGroup('Cost', 'cost', errors.cost && errors.cost.message, 'number', register)}
        {InputGroup('Region', 'region', errors.region && errors.region.message, 'text', register)}
        {InputGroup('Sale', 'sale', errors.sale && errors.sale.message, 'number', register)}
        {InputGroup('Type', 'type', errors.type && errors.type.message, 'text', register)}
        {InputGroup('Description', 'description', errors.description && errors.description.message, 'text', register)}
        {InputGroup('Short description', 'shotDesc', errors.shotDesc && errors.shotDesc.message, 'text', register)}
        {InputGroup('Map', 'map', errors.map && errors.map.message, 'text', register)}
        {InputGroup('Ending', 'ending', errors.ending && errors.ending.message, 'text', register)}
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">
          {stateCreateQuest.isFetching ?
            <CircularProgress size={24} /> : 'Create'}
        </Button>
      </form>
    </div>
  );
};

export default QuestsCreate;