import React from 'react';
import { useSelector } from 'react-redux';
import { State } from '../../reducers';
import TableSortPagination from '../TableSortPagination';
import { TableRow, TableCell } from '@material-ui/core';
import { Quest } from '../../models';

const headCells = [
  { id: 'title', align: 'left', label: 'Title' },
  { id: 'shotDesc', align: 'left', label: 'Short desc' },
  { id: 'address', align: 'left', label: 'Address' },
  { id: 'cost', align: 'left', label: 'Cost' },
  { id: 'sale', align: 'left', label: 'Sale' },
  { id: 'type', align: 'left', label: 'Type' },
];

const QuestsList = () => {
  const stateQuests = useSelector((state: State) => state.stateQuests);

  return (
    <TableSortPagination
      headCells={headCells}
      bodyRows={stateQuests.questsList}
      defaultSort={'fullname'}
      getBodyRow={(quest: Quest) => (
        <TableRow key={quest.questId}>
          <TableCell component="th" scope="row">{quest.title}</TableCell>
          <TableCell align="right">{quest.shotDesc}</TableCell>
          <TableCell align="right">{quest.address}</TableCell>
          <TableCell align="right">{quest.cost}</TableCell>
          <TableCell align="right">{quest.sale}</TableCell>
          <TableCell align="right">{quest.type}</TableCell>
        </TableRow>
      )}
    />
  );
};

export default QuestsList;