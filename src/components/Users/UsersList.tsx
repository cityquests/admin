import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import { fetchGetUsersAction } from '../../actions';
import TableSortPagination from '../TableSortPagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const headCells = [
  { id: 'fullname', align: 'left', label: 'Full name' },
  { id: 'email', align: 'right', label: 'Email' },
  { id: 'phone', align: 'right', label: 'Phone' },
  { id: 'lastLogin', align: 'left', label: 'Last login' },
  { id: 'description', align: 'left', label: 'Description' },
];

const UsersList = () => {
  const dispatch = useDispatch();
  const stateUsers = useSelector((state: State) => state.stateUsers);
  useEffect(() => {
    dispatch(fetchGetUsersAction.request());
  }, [dispatch]);

  return (
    <TableSortPagination
      headCells={headCells}
      bodyRows={stateUsers.users_list}
      defaultSort={'fullname'}
      getBodyRow={(user: any) => (
        <TableRow key={user.email}>
          <TableCell component="th" scope="row">{user.fullname}</TableCell>
          <TableCell align="right">{user.email}</TableCell>
          <TableCell align="right">{user.phone}</TableCell>
          <TableCell align="right">{user.lastLogin}</TableCell>
          <TableCell align="right">{user.description}</TableCell>
        </TableRow>
      )}
    />
  );
};

export default UsersList;