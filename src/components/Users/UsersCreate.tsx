import React from 'react';
import { useDispatch } from 'react-redux';
import {
  fetchPostUserAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { User } from '../../models';
import { InputGroup } from '../Form';
import Button from '@material-ui/core/Button';

const UsersCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<User>();
  const onSubmit = (data: User) => {
    dispatch(fetchPostUserAction.request(data));
  };
  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('E-mail', 'email', 
          errors.email && errors.email.message, 'email', register)}
        {InputGroup('Password', 'password', 
          errors.password && errors.password.message, 'password', register)}
        {InputGroup('Nickname', 'nickname',
          errors.nickname && errors.nickname.message, 'text', register)}
        {InputGroup('Phone', 'phone', 
          errors.phone && errors.phone.message, 'text', register, false)}
        {InputGroup('Description', 'description', 
          errors.description && errors.description.message, 'text', register, false)}
        {InputGroup('Fullname', 'fullname',
          errors.fullname && errors.fullname.message, 'text', register, false)}
        {InputGroup('Birthday', 'birthday',
          errors.birthday && errors.birthday.message, 'date', register, false, {
            InputLabelProps: {
              shrink: true,
            },
          })}
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">Create</Button>
      </form>
    </div>
  );
};

export default UsersCreate;