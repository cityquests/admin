import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchPostStaffAction,
  fetchGetStaffCategoriesAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { State } from '../../reducers';
import { Staff } from '../../models';
import { InputGroup } from '../Form';
import {
  Button,
  Select, MenuItem,
} from '@material-ui/core';

const StaffsCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<Staff>();
  const [category, setCategory] = useState<string>('');
  const onSubmit = (data: Staff) => {
    dispatch(fetchPostStaffAction.request({
      ...data,
      category: category,
    }));
  };
  const { staffCategories_list } = useSelector((state: State) => state.stateStaffCategories);
  useEffect(() => {
    dispatch(fetchGetStaffCategoriesAction.request());
  }, [dispatch]);

  useEffect(() => {
    if (category === '' && staffCategories_list.length !== 0) {
      setCategory(staffCategories_list[0].type);
    }
  }, [dispatch, category, staffCategories_list]);
  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Name', 'name', errors.name && errors.name.message, 'text', register)}
        {InputGroup('E-mail', 'email', errors.email && errors.email.message, 'email', register)}
        {InputGroup('Password', 'password', errors.password && errors.password.message, 'text', register)}
        <div className="">
          <label>Staff Category</label>
          <Select
            ref={register({ required: ' is required' })}
            onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
              setCategory(v.target.value as string);
            }}
            value={category}
            name="category"
            className="select-form"
          >
            {staffCategories_list.map(category => {
              return <MenuItem key={category.type} value={category.type}>{category.displayName}</MenuItem>;
            })}
          </Select>
        </div>
        {InputGroup('Phone', 'phone', errors.phone && errors.phone.message, 'text', register)}
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">Create</Button>
      </form>
    </div >
  );
};

export default StaffsCreate;