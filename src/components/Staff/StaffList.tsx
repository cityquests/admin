import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import { fetchGetStaffsAction } from '../../actions';
import TableSortPagination from '../TableSortPagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const headCells = [
  { id: 'name', align: 'left', label: 'Name' },
  { id: 'email', align: 'left', label: 'Email' },
  { id: 'phone', align: 'left', label: 'Phone' },
  { id: 'category', align: 'left', label: 'Category' },
  { id: 'lastLoging', align: 'left', label: 'Last Login' },
];

const UsersList = () => {
  const dispatch = useDispatch();
  const stateStaffs = useSelector((state: State) => state.stateStaffs);
  useEffect(() => {
    dispatch(fetchGetStaffsAction.request());
  }, [dispatch]);

  return (
    <TableSortPagination
      headCells={headCells}
      bodyRows={stateStaffs.staffs_list}
      defaultSort={'name'}
      getBodyRow={(staffCategory: any) => (
        <TableRow key={staffCategory.email}>
          <TableCell scope="row">{staffCategory.name}</TableCell>
          <TableCell align="left">{staffCategory.email}</TableCell>
          <TableCell align="left">{staffCategory.phone}</TableCell>
          <TableCell align="left">{staffCategory.category}</TableCell>
          <TableCell align="left">{staffCategory.lastLoging}</TableCell>
        </TableRow>
      )}
    />
  );
};

export default UsersList;