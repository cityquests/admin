import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import { fetchGetStaffCategoriesAction } from '../../actions';
import TableSortPagination from '../TableSortPagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const headCells = [
  { id: 'displayName', align: 'left', label: 'Name' },
  { id: 'privileges', align: 'left', label: 'Privileges' },
  { id: 'type', align: 'left', label: 'Type' },
];

const StaffCategoryList = () => {
  const dispatch = useDispatch();
  const stateStaffCategories = useSelector((state: State) => state.stateStaffCategories);
  useEffect(() => {
    dispatch(fetchGetStaffCategoriesAction.request());
  }, [dispatch]);

  return (
    <TableSortPagination
      headCells={headCells}
      bodyRows={stateStaffCategories.staffCategories_list}
      defaultSort={'fullname'}
      getBodyRow={(staffCategory: any) => (
        <TableRow key={staffCategory.displayName}>
          <TableCell scope="row">{staffCategory.displayName}</TableCell>
          <TableCell align="left">{staffCategory.privileges}</TableCell>
          <TableCell align="left">{staffCategory.type}</TableCell>
        </TableRow>
      )}
    />
  );
};

export default StaffCategoryList;