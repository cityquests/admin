import React from 'react';
import { useDispatch } from 'react-redux';
import {
  fetchPostStaffCategoryAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { StaffCategory } from '../../models';
import { InputGroup } from '../Form';
import Button from '@material-ui/core/Button';

const StaffCategorysCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<StaffCategory>();
  const onSubmit = (data: StaffCategory) => {
    dispatch(fetchPostStaffCategoryAction.request(data));
  };
  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Display Name', 'displayName', errors.displayName && errors.displayName.message, 'text', register)}
        {InputGroup('Privileges', 'privileges', errors.privileges && errors.privileges.message, 'text', register)}
        {InputGroup('Type', 'type', errors.type && errors.type.message, 'text', register)}
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">Create</Button>
      </form>
    </div>
  );
};

export default StaffCategorysCreate;