import React, { useEffect, useState } from 'react';
import AppConfig from '../../config';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import {
  fetchGetImagesAction,
  fetchGetQuestionTypesAction,
  fetchGetQuestionAreasAction,
  fetchDeleteImageAction,
} from '../../actions';
import './ImagesList.css';
import GoogleMapReact from 'google-map-react';
import {
  FileCopy as FileCopyIcon,
  Edit as EditIcon,
  DeleteForever as DeleteForeverIcon,
} from '@material-ui/icons';
import { Image } from '../../models';
import {
  Button, CircularProgress,
  Dialog, DialogActions,
  DialogContent,
  DialogContentText, DialogTitle,
  Select, MenuItem,
} from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';

const ImagesList = () => {
  const [filterArea, setFilterArea] = useState(0);
  const [activeImage, setActiveImage] = useState<Image>();
  const [openDeleteDialog, setOpenDeleteDialog] = React.useState(false);
  const dispatch = useDispatch();
  const stateImages = useSelector((state: State) => state.stateImages);
  const stateQuestionAreas = useSelector((state: State) => state.stateQuestionAreas);
  const stateDeleteImage = useSelector((state: State) => state.deleteImage);
  useEffect(() => {
    dispatch(fetchGetQuestionTypesAction.request());
    dispatch(fetchGetQuestionAreasAction.request());
  }, [dispatch]);
  useEffect(() => {
    if (stateQuestionAreas.question_area_list.length !== 0) {
      setFilterArea(stateQuestionAreas.question_area_list[0].areaId);
    }
  }, [stateQuestionAreas.question_area_list]);
  useEffect(() => {
    if (filterArea !== 0) {
      dispatch(fetchGetImagesAction.request({ areaId: filterArea, page: 1 }));
    }
  }, [dispatch, filterArea]);
  const defaultProps = {
    center: {
      lat: 35.7,
      lng: 139.8,
    },
    zoom: 11,
  };
  const AnyReactComponent = ({ text, image }: any) =>
    <div
      onClick={() =>
        setActiveImage(activeImage && activeImage.imageId === image.imageId ? undefined : image)
      }
      className={`marker ${activeImage && activeImage.imageId === image.imageId ? 'marker--active' : ''}`}
    >{text}</div>;

  const onImageCopy = (image: Image) => {
    const textField = document.createElement('textarea');
    textField.innerText = image.url.includes('http') ? image.url : `${AppConfig.IMAGES_URL}${image.url}`;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
  };

  const handleClickOpenDeleteDialog = (image: Image) => {
    setActiveImage(image);
    setOpenDeleteDialog(true);
  };

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false);
  };

  const handleDeleteImage = () => {
    if (activeImage) {
      setOpenDeleteDialog(false);
      dispatch(fetchDeleteImageAction.request({ imageId: activeImage._id, areaId: activeImage.areaId }));
    }
  };

  const handlePaginationChange = (page: number) => {
    if (page !== stateImages.currentPage) {
      dispatch(fetchGetImagesAction.request({ areaId: filterArea, page: page }));
    }
  };

  return (
    <div className="box-body image-list-page">
      <div className="nav_filter">
        <Select
          value={filterArea}
          onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
            setFilterArea(parseInt(e.target.value as string))}>
          {stateQuestionAreas.question_area_list.map(area =>
            <MenuItem key={area.areaId} value={area.areaId}>{area.name}</MenuItem>,
          )}
        </Select>
      </div>
      <br />
      <div style={{ height: '300px', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: AppConfig.GOOGLE_MAP_API }}
          defaultCenter={defaultProps.center}
          yesIWantToUseGoogleMapApiInternals
          defaultZoom={defaultProps.zoom}
        >
          {stateImages.imagesList.map(image => {
            return (
              <AnyReactComponent
                key={image.imageId}
                lat={image.location[0]}
                lng={image.location[1]}
                text={image.name}
                image={image}
              />
            );
          })}
        </GoogleMapReact>
      </div>
      <br />
      <div className="image_list">
        {stateImages.isFetching ? <CircularProgress size={20} /> :
          stateImages.imagesList.map(image => {
            const imageName = stateQuestionAreas.question_area_list.filter(a => a.areaId === image.areaId)[0].name;
            return (
              <div
                key={image.imageId}
                className={`${activeImage &&
                  activeImage.imageId === image.imageId ?
                  'image_item--active' : ''} image_item`}
              >
                <img
                  onClick={() =>
                    setActiveImage(activeImage && activeImage.imageId === image.imageId ? undefined : image)
                  }
                  alt={image.name}
                  src={image.url.includes('http') ? image.url : `${AppConfig.IMAGES_URL}${image.url}`}
                  className="image_item__images" />
                <div className="image_item__info">
                  <span className="image_item__info--detail">
                    {image.name} | {imageName}
                  </span>
                  <span className="image_item__info--actions">
                    <FileCopyIcon color="action" onClick={() => onImageCopy(image)} />
                    <EditIcon color="primary" />
                    {
                      activeImage && activeImage.imageId === image.imageId &&
                        stateDeleteImage.isFetching ? <CircularProgress size={15} /> :
                        <DeleteForeverIcon color="error" onClick={() => handleClickOpenDeleteDialog(image)} />
                    }
                  </span>
                </div>
              </div>
            );
          })}
        <Pagination
          onChange={(event: object, page: number) => handlePaginationChange(page)}
          count={stateImages.pages}
          page={stateImages.currentPage}
          showFirstButton
          showLastButton />
      </div>
      <Dialog
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Are you sure want to delete this image?</DialogTitle>
        {activeImage ? (
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {activeImage.name}
              <img
                alt={activeImage.name}
                src={activeImage.url.includes('http') ?
                  activeImage.url : `${AppConfig.IMAGES_URL}${activeImage.url}`}
                className="image_item__images" />
            </DialogContentText>
          </DialogContent>
        ) : ''}
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary" autoFocus>
            Cancel
          </Button>
          <Button onClick={handleDeleteImage} color="secondary">
            DELETE
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ImagesList;