import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import LocationPicker from 'react-location-picker';
import {
  fetchGetQuestionTypesAction,
  fetchGetQuestionAreasAction,
  fetchPostImageAction,
} from '../../actions';
import { State } from '../../reducers';
import { useForm } from 'react-hook-form';
import { Image } from '../../models';
import { InputGroup, FileGroup } from '../Form';
import {
  Button, CircularProgress,
  Select, MenuItem,
} from '@material-ui/core';
import heic2any from 'heic2any';
import imageCompression from 'browser-image-compression';

const ImagesCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<Image>();
  const stateCreateImage = useSelector((state: State) => state.createImage);
  const stateQuestionAreas = useSelector((state: State) => state.stateQuestionAreas);
  const stateQuestionTypes = useSelector((state: State) => state.stateQuestionTypes);
  useEffect(() => {
    dispatch(fetchGetQuestionTypesAction.request());
    dispatch(fetchGetQuestionAreasAction.request());
  }, [dispatch]);
  const onSubmit = (data: Image) => {
    // @ts-ignore
    if (data.url[0].type.includes('heic')) {
      heic2any({
        // @ts-ignore
        blob: data.url[0],
      }).then((res: any) => {
        imageComp(res, data);
      });
    } else {
      // @ts-ignore
      imageComp(data.url[0], data);
    }
  };

  const imageComp = (blobData: Blob, data: Image) => {
    const options = {
      maxSizeMB: 1,
      maxWidthOrHeight: 1920,
      useWebWorker: true,
    };
    imageCompression(blobData, options)
      .then((compressedFile: any) => {
        const fd = new FormData();
        fd.set('a', compressedFile);
        // @ts-ignore
        data.url = fd.get('a');
        data.areaId = parseInt(areaId);
        data.typeId = parseInt(typeId);
        postImage(data);
      })
      .catch((error: Error) => {
        alert(error.message);
      });
  };

  const postImage = (data: Image) => {
    dispatch(fetchPostImageAction.request({
      ...data,
      location: [locationPicker.position.lat, locationPicker.position.lng],
    }));
  };

  const [locationPicker, setLocationPicker] = useState({
    position: {
      lat: 0,
      lng: 0,
    },
  });
  const [areaId, setAreaId] = useState<string>('');
  const [typeId, setTypeId] = useState<string>('');


  useEffect(() => {
    if (areaId === '' && stateQuestionAreas.question_area_list.length !== 0) {
      setAreaId(stateQuestionAreas.question_area_list[0].areaId.toString());
    }
  }, [dispatch, areaId, stateQuestionAreas.question_area_list]);

  useEffect(() => {
    if (typeId === '' && stateQuestionTypes.question_type_list.length !== 0) {
      setTypeId(stateQuestionTypes.question_type_list[0].typeId.toString());
    }
  }, [dispatch, typeId, stateQuestionTypes.question_type_list]);

  useEffect(() => {
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        setLocationPicker({ position: pos });
      });
    }
  }, []);

  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Name', 'name', errors.name && errors.name.message, 'text', register)}
        <div className="">
          <label>Area</label>
          <label style={{ color: 'red' }}>
            {errors.areaId}
          </label>
          <Select
            className="select-form"
            name="areaId"
            onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
              setAreaId(v.target.value as string);
            }}
            value={areaId}
          >
            {stateQuestionAreas.question_area_list.map(area =>
              <MenuItem key={area.areaId} value={area.areaId}>{area.name}</MenuItem>,
            )}
          </Select>
        </div>
        <div className="">
          <label>Type</label>
          <label style={{ color: 'red' }}>
            {errors.typeId}
          </label>
          <Select
            className="select-form"
            name="typeId"
            onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
              setTypeId(v.target.value as string);
            }}
            value={typeId}
          >
            {stateQuestionTypes.question_type_list.map(type =>
              <MenuItem key={type.typeId} value={type.typeId}>{type.name}</MenuItem>,
            )}
          </Select>
        </div>
        {FileGroup('Upload image', 'url', errors.url && errors.url.message, register)}
        <div>
          <LocationPicker
            containerElement={<div style={{ height: '100%' }} />}
            mapElement={<div style={{ height: '400px' }} />}
            defaultPosition={locationPicker.position}
            onChange={({ position }: any) => {
              setLocationPicker({ position });
            }}
            radius={2}
            zoom={15}
          />
        </div>
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit"
          disabled={stateCreateImage.isFetching}
        >
          {stateCreateImage.isFetching ?
            <CircularProgress size={24} /> : 'Create'}
        </Button>
      </form>
    </div>
  );
};

export default ImagesCreate;