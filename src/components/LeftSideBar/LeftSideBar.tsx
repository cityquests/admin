import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { State } from '../../reducers';
import history from '../../history';
import clsx from 'clsx';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import {
  Drawer, List, IconButton,
  ListItem, ListItemIcon, ListItemText,
} from '@material-ui/core';
import {
  ChevronRight as ChevronRightIcon, PhotoLibrary as PhotoLibraryIcon,
  AddAPhoto as AddAPhotoIcon, People as PeopleIcon, GroupAdd as GroupAddIcon,
  SportsEsports as SportsEsportsIcon, Gamepad as GamepadIcon,
  QuestionAnswer as QuestionAnswerIcon, Help as HelpIcon, AddToPhotos as AddToPhotosIcon,
  LowPriority as LowPriorityIcon, PersonAdd as PersonAddIcon, AssignmentInd as AssignmentIndIcon,
  SupervisedUserCircle as SupervisedUserCircleIcon, RecentActors as RecentActorsIcon,
  AccountTree as AccountTreeIcon, Add as AddIcon, Ballot as BallotIcon, PostAdd as PostAddIcon, 
  ChevronLeft as ChevronLeftIcon, 
} from '@material-ui/icons';
const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

interface LeftSideBar {
  drawerIsOpen: boolean;
  handleDrawerClose: any;
};

const LeftSideBar: React.FC<LeftSideBar> = ({ drawerIsOpen, handleDrawerClose }) => {
  const classes = useStyles();
  const theme = useTheme();
  const { permissions } = useSelector((state: State) => state.statePermissions);
  const [location, setLocation] = useState(window.location.href);
  useEffect(() => {
    if (location !== window.location.href) {
      history.push(location);
    }
  }, [location]);
  return (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: drawerIsOpen,
        [classes.drawerClose]: !drawerIsOpen,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: drawerIsOpen,
          [classes.drawerClose]: !drawerIsOpen,
        }),
      }}
    >
      <div className={classes.toolbar}>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
        </IconButton>
      </div>
      <List>
        {!!permissions['image:create'] && <ListItem
          selected={location.includes('images/new')}
          onClick={() => setLocation('/images/new')}
          button>
          <ListItemIcon><AddAPhotoIcon /></ListItemIcon>
          <ListItemText primary='New image' />
        </ListItem>}
        {!!permissions['image:view'] && <ListItem
          selected={location.includes('images/list')}
          onClick={() => setLocation('/images/list')}
          button>
          <ListItemIcon><PhotoLibraryIcon /></ListItemIcon>
          <ListItemText primary='Images list' />
        </ListItem>}
      </List>
      {(!!permissions['user:create'] || !!permissions['user:view']) && <List>
        {!!permissions['user:create'] && <ListItem
          selected={location.includes('users/new')}
          onClick={() => setLocation('/users/new')}
          button>
          <ListItemIcon><GroupAddIcon /></ListItemIcon>
          <ListItemText primary='New user' />
        </ListItem>}
        {!!permissions['user:view'] && <ListItem
          selected={location.includes('users/list')}
          onClick={() => setLocation('/users/list')}
          button>
          <ListItemIcon><PeopleIcon /></ListItemIcon>
          <ListItemText primary='Users list' />
        </ListItem>}
      </List>}
      {(!!permissions['quest:create'] || !!permissions['quest:view']) && <List>
        {!!permissions['quest:create'] && <ListItem
          selected={location.includes('quests/new')}
          onClick={() => setLocation('/quests/new')}
          button>
          <ListItemIcon><GamepadIcon /></ListItemIcon>
          <ListItemText primary='New quest' />
        </ListItem>}
        {!!permissions['quest:view'] && <ListItem
          selected={location.includes('quests/list')}
          onClick={() => setLocation('/quests/list')}
          button>
          <ListItemIcon><SportsEsportsIcon /></ListItemIcon>
          <ListItemText primary='Quests list' />
        </ListItem>}
      </List>}
      {(!!permissions['question:create'] || !!permissions['question:view']) && <List>
        {!!permissions['question:create'] && <ListItem
          selected={location.includes('questions/new')}
          onClick={() => setLocation('/questions/new')}
          button>
          <ListItemIcon><HelpIcon /></ListItemIcon>
          <ListItemText primary='New question' />
        </ListItem>}
        {!!permissions['question:view'] && <ListItem
          selected={location.includes('questions/list')}
          onClick={() => setLocation('/questions/list')}
          button>
          <ListItemIcon><QuestionAnswerIcon /></ListItemIcon>
          <ListItemText primary='Questions list' />
        </ListItem>}
      </List>}
      {(!!permissions['hint:create'] || !!permissions['hint:view']) && <List>
        {!!permissions['hint:create'] && <ListItem
          selected={location.includes('hints/new')}
          onClick={() => setLocation('/hints/new')}
          button>
          <ListItemIcon><AddToPhotosIcon /></ListItemIcon>
          <ListItemText primary='New hint' />
        </ListItem>}
        {!!permissions['hint:view'] && <ListItem
          selected={location.includes('hints/list')}
          onClick={() => setLocation('/hints/list')}
          button>
          <ListItemIcon><LowPriorityIcon /></ListItemIcon>
          <ListItemText primary='Hints list' />
        </ListItem>}
      </List>}
      {(!!permissions['staff:create'] || !!permissions['staff:view']) && <List>
        {!!permissions['staff:create'] && <ListItem
          selected={location.includes('staff/new')}
          onClick={() => setLocation('/staff/new')}
          button>
          <ListItemIcon><PersonAddIcon /></ListItemIcon>
          <ListItemText primary='New staff' />
        </ListItem>}
        {!!permissions['staff:view'] && <ListItem
          selected={location.includes('staff/list')}
          onClick={() => setLocation('/staff/list')}
          button>
          <ListItemIcon><AssignmentIndIcon /></ListItemIcon>
          <ListItemText primary='Staff list' />
        </ListItem>}
      </List>}
      {(!!permissions['staff_categories:create'] || !!permissions['staff_categories:view']) && <List>
        {!!permissions['staff_categories:create'] && <ListItem
          selected={location.includes('staffcategory/new')}
          onClick={() => setLocation('/staffcategory/new')}
          button>
          <ListItemIcon><RecentActorsIcon /></ListItemIcon>
          <ListItemText primary='New staff category' />
        </ListItem>}
        {!!permissions['staff_categories:view'] && <ListItem
          selected={location.includes('staffcategory/list')}
          onClick={() => setLocation('/staffcategory/list')}
          button>
          <ListItemIcon><SupervisedUserCircleIcon /></ListItemIcon>
          <ListItemText primary='Staff Category list' />
        </ListItem>}
      </List>}
      {(!!permissions['q_area:create'] || !!permissions['q_area:view']) && <List>
        {!!permissions['q_area:create'] && <ListItem
          selected={location.includes('questionarea/new')}
          onClick={() => setLocation('/questionarea/new')}
          button>
          <ListItemIcon><AddIcon /></ListItemIcon>
          <ListItemText primary='New question area' />
        </ListItem>}
        {!!permissions['q_area:view'] && <ListItem
          selected={location.includes('questionarea/list')}
          onClick={() => setLocation('/questionarea/list')}
          button>
          <ListItemIcon><AccountTreeIcon /></ListItemIcon>
          <ListItemText primary='Question area list' />
        </ListItem>}
      </List>}
      {(!!permissions['q_type:create'] || !!permissions['q_type:view']) && <List>
        {!!permissions['q_type:create'] && <ListItem
          selected={location.includes('questiontype/new')}
          onClick={() => setLocation('/questiontype/new')}
          button>
          <ListItemIcon><PostAddIcon /></ListItemIcon>
          <ListItemText primary='New question type' />
        </ListItem>}
        {!!permissions['q_type:view'] && <ListItem
          selected={location.includes('questiontype/list')}
          onClick={() => setLocation('/questiontype/list')}
          button>
          <ListItemIcon><BallotIcon /></ListItemIcon>
          <ListItemText primary='Question type list' />
        </ListItem>}
      </List>}
    </Drawer>
  );
};

export default LeftSideBar;
// export default withRouter(LeftSideBar);