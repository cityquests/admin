import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchPostHintAction,
  fetchGetQuestionsAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { State } from '../../reducers';
import { Hint } from '../../models';
import { InputGroup, FileGroup } from '../Form';
import {
  Button, CircularProgress,
  Select, MenuItem,
} from '@material-ui/core';

const HintsCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<Hint>();
  const [questId, setQuestId] = useState<string>('');
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [questionId, setQuestionId] = useState<string>('');
  const onSubmit = (data: Hint) => {
    const fd = new FormData();
    fd.set('a', data.img[0]);
    dispatch(fetchPostHintAction.request({
      ...data,
      questionId: questionId,
      // @ts-ignore
      img: fd.get('a'),
    }));
  };
  const stateCreateHint = useSelector((state: State) => state.createHint);
  const { questionsList } = useSelector((state: State) => state.stateQuestions);
  const { questsList } = useSelector((state: State) => state.stateQuests);

  useEffect(() => {
    if (questId === '' && questsList.length !== 0) {
      setQuestId(questsList[0].questId);
    }
  }, [dispatch, questId, questsList]);

  useEffect(() => {
    if (questionId === '' && questionsList.length) {
      setQuestionId(questionsList[0].questionId);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, questionsList]);

  useEffect(() => {
    if (questId !== '') {
      dispatch(fetchGetQuestionsAction.request({ questId: questId }));
    }
  }, [dispatch, questId]);

  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Description', 'description', errors.description && errors.description.message, 'text', register)}
        {FileGroup('Img', 'img', errors.img && errors.img.message, register)}
        {InputGroup('Order', 'order', errors.order && errors.order.message, 'number', register)}
        {InputGroup('Type', 'type', errors.type && errors.type.message, 'text', register)}
        <div className="">
          <label>Quest</label>
          <Select
            className="select-form"
            onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
              setQuestionId('' as string);
              setQuestId(v.target.value as string);
            }}
            value={questId}
            name="questId">
            {questsList.map(quest => {
              return <MenuItem key={quest.questId} value={quest.questId}>{quest.title}</MenuItem>;
            })}
          </Select>
        </div>
        <div className="">
          <label>Question</label>
          <Select
            ref={register({ required: ' is required' })}
            className="select-form"
            onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
              setQuestionId(v.target.value as string);
            }}
            value={questionId}
            name="questionId">
            {questionsList.map(question => {
              return <MenuItem key={question.questionId} value={question.questionId}>{question.description}</MenuItem>;
            })}
          </Select>
        </div>
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">
          {stateCreateHint.isFetching ?
            <CircularProgress size={24} /> : 'Create'}
        </Button>
      </form>
    </div>
  );
};

export default HintsCreate;