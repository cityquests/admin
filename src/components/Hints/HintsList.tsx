import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import {
  fetchGetHintsAction,
  fetchGetQuestionsAction,
} from '../../actions';
import TableSortPagination from '../TableSortPagination';
import {
  TableRow, TableCell,
  Select, MenuItem, CircularProgress,
} from '@material-ui/core';
import { Hint } from '../../models';

const headCells = [
  { id: 'description', align: 'left', label: 'Description' },
  { id: 'type', align: 'left', label: 'type' },
  { id: 'order', align: 'left', label: 'Order' },
];

const HintsList = () => {
  const dispatch = useDispatch();
  const [questId, setQuestId] = useState<string>('');
  const [questionId, setQuestionId] = useState<string>('');
  const stateHints = useSelector((state: State) => state.stateHints);
  const questionsState = useSelector((state: State) => state.stateQuestions);
  const questsState = useSelector((state: State) => state.stateQuests);

  useEffect(() => {
    if (questId === '' && questsState.questsList.length !== 0) {
      setQuestId(questsState.questsList[0].questId);
    }
  }, [dispatch, questId, questsState.questsList]);
  useEffect(() => {
    if (questionId === '' && questionsState.questionsList.length) {
      setQuestionId(questionsState.questionsList[0].questionId);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, questionsState.questionsList]);

  useEffect(() => {
    if (questId !== '') {
      dispatch(fetchGetQuestionsAction.request({ questId: questId }));
    }
  }, [dispatch, questId]);

  useEffect(() => {
    if (questionId !== '') {
      dispatch(fetchGetHintsAction.request({ questionId: questionId }));
    }
  }, [dispatch, questionId]);

  return (
    <>
      <div className="inlineblock">
        <label>Quest</label>
        <Select
          className="select-form"
          onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
            v.preventDefault();
            setQuestionId('' as string);
            setQuestId(v.target.value as string);
          }}
          value={questId}
          name="questId">
          {questsState.questsList.map(quest => {
            return <MenuItem key={quest.questId} value={quest.questId}>{quest.title}</MenuItem>;
          })}
        </Select>
      </div>
      <div className="inlineblock">
        <label>Question</label>
        <Select
          className="select-form"
          onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
            v.preventDefault();
            setQuestionId(v.target.value as string);
          }}
          value={questionId}
          name="questionId">
          {questionsState.questionsList.map(question => {
            return <MenuItem key={question.questionId} value={question.questionId}>{question.description}</MenuItem>;
          })}
        </Select>
      </div>
      {questsState.isFetching || questionsState.isFetching ? (
        <CircularProgress size={24} />
      ) : (
        <TableSortPagination
          headCells={headCells}
          bodyRows={stateHints.hintsList}
          defaultSort={'fullname'}
          getBodyRow={(hint: Hint) => (
            <TableRow key={hint.hintId}>
              <TableCell scope="row">{hint.description}</TableCell>
              <TableCell align="left">{hint.type}</TableCell>
              <TableCell align="left">{hint.order}</TableCell>
            </TableRow>
          )}
        />
      )}
    </>
  );
};

export default HintsList;