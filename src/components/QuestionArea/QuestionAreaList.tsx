import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import { fetchGetQuestionAreasAction } from '../../actions';
import TableSortPagination from '../TableSortPagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const headCells = [
  { id: 'areaId', align: 'left', label: 'Area Id' },
  { id: 'name', align: 'left', label: 'Name' },
];

const StaffCategoryList = () => {
  const dispatch = useDispatch();
  const stateQuestionAreas = useSelector((state: State) => state.stateQuestionAreas);
  useEffect(() => {
    dispatch(fetchGetQuestionAreasAction.request());
  }, [dispatch]);

  return (
    <TableSortPagination
      headCells={headCells}
      bodyRows={stateQuestionAreas.question_area_list}
      defaultSort={'name'}
      getBodyRow={(questionArea: any) => (
        <TableRow key={questionArea.name}>
          <TableCell scope="row">{questionArea.areaId}</TableCell>
          <TableCell align="left">{questionArea.name}</TableCell>
        </TableRow>
      )}
    />
  );
};

export default StaffCategoryList;