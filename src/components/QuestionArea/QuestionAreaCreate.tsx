import React from 'react';
import { useDispatch } from 'react-redux';
import {
  fetchPostQuestionAreaAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { QuestionArea } from '../../models';
import { InputGroup } from '../Form';
import Button from '@material-ui/core/Button';

const QuestionAreasCreate = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm<QuestionArea>();
  const onSubmit = (data: QuestionArea) => {
    dispatch(fetchPostQuestionAreaAction.request(data));
  };
  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Area Id', 'areaId',
          errors.areaId && errors.areaId.message, 'text', register)}
        {InputGroup('Name', 'name',
          errors.name && errors.name.message, 'text', register)}
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">Create</Button>
      </form>
    </div>
  );
};

export default QuestionAreasCreate;