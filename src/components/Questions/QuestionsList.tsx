import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../reducers';
import {
  fetchGetQuestionsAction,
} from '../../actions';
import { Question } from '../../models';
import TableSortPagination from '../TableSortPagination';
import {
  TableRow, TableCell,
  Select, MenuItem,
} from '@material-ui/core';

const headCells = [
  { id: 'description', align: 'left', label: 'Description' },
  { id: 'answer', align: 'left', label: 'Answer' },
  { id: 'fact', align: 'left', label: 'Fact' },
  { id: 'type', align: 'left', label: 'Type' },
  { id: 'order', align: 'left', label: 'Order' },
];

const QuestionsList = () => {
  const dispatch = useDispatch();
  const stateQuestions = useSelector((state: State) => state.stateQuestions);
  const { questsList } = useSelector((state: State) => state.stateQuests);
  const [questId, setQuestId] = useState<string>('');

  useEffect(() => {
    if (questId === '' && questsList.length !== 0) {
      setQuestId(questsList[0].questId);
    }
  }, [dispatch, questId, questsList]);

  useEffect(() => {
    if (questId !== '') {
      dispatch(fetchGetQuestionsAction.request({ questId: questId }));
    }
  }, [dispatch, questId]);

  return (
    <>
      <div className="">
        <label>Quest</label>
        <Select
          className="select-form"
          onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
            setQuestId(v.target.value as string);
          }}
          value={questId}
          name="questId">
          {questsList.map(quest => {
            return <MenuItem key={quest.questId} value={quest.questId}>{quest.title}</MenuItem>;
          })}
        </Select>
      </div>
      <TableSortPagination
        headCells={headCells}
        bodyRows={stateQuestions.questionsList}
        defaultSort={'fullname'}
        getBodyRow={(question: Question) => (
          <TableRow key={question.questionId}>
            <TableCell scope="row">{question.description}</TableCell>
            <TableCell align="left">{question.answer}</TableCell>
            <TableCell align="left">{question.fact}</TableCell>
            <TableCell align="left">{question.type}</TableCell>
            <TableCell align="left">{question.order}</TableCell>
          </TableRow>
        )}
      />
    </>
  );
};

export default QuestionsList;