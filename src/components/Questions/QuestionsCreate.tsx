import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchPostQuestionAction,
} from '../../actions';
import { useForm } from 'react-hook-form';
import { State } from '../../reducers';
import { Question } from '../../models';
import { InputGroup, FileGroup } from '../Form';
import {
  Button, CircularProgress,
  Select, MenuItem,
} from '@material-ui/core';

const QuestionsCreate = () => {
  const dispatch = useDispatch();
  const [questId, setQuestId] = useState<string>('');
  const { register, handleSubmit, errors } = useForm<Question>();
  const stateCreateQuestion = useSelector((state: State) => state.createQuestion);
  const onSubmit = (data: Question) => {
    const fd = new FormData();
    fd.set('a', data.img[0]);
    dispatch(fetchPostQuestionAction.request({
      ...data,
      questId: questId,
      // @ts-ignore
      img: fd.get('a'),
    }));
  };
  const { questsList } = useSelector((state: State) => state.stateQuests);

  useEffect(() => {
    if (questId === '' && questsList.length !== 0) {
      setQuestId(questsList[0].questId);
    }
  }, [dispatch, questId, questsList]);

  return (
    <div className="box-body">
      <form onSubmit={handleSubmit(onSubmit)}>
        {InputGroup('Description', 'description', errors.description && errors.description.message, 'text', register)}
        {InputGroup('Answer', 'answer', errors.answer && errors.answer.message, 'text', register)}
        {InputGroup('Fact', 'fact', errors.fact && errors.fact.message, 'text', register)}
        {InputGroup('Order', 'order', errors.order && errors.order.message, 'number', register)}
        {FileGroup('Img', 'img', errors.img && errors.img.message, register)}
        {InputGroup('Map', 'map', errors.map && errors.map.message, 'text', register)}
        {InputGroup('Type', 'type', errors.type && errors.type.message, 'text', register)}
        <div className="">
          <label>Quest Id</label>
          <Select
            ref={register({ required: ' is required' })}
            value={questId}
            className="select-form"
            onChange={(v: React.ChangeEvent<{ value: unknown }>) => {
              setQuestId(v.target.value as string);
            }}
            name="questId">
            {questsList.map(quest => {
              return <MenuItem key={quest.questId} value={quest.questId}>{quest.title}</MenuItem>;
            })}
          </Select>
        </div>
        <br />
        <Button
          variant="contained"
          color="primary"
          type="submit">
          {stateCreateQuestion.isFetching ?
            <CircularProgress size={24} /> : 'Create'}
        </Button>
      </form>
    </div>
  );
};

export default QuestionsCreate;