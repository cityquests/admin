import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router-dom';
import {
  fetchGetQuestsAction,
  fetchPermissionsAction,
} from '../../actions';
import { UsersList, UsersCreate } from '../../components/Users';
import { QuestsList, QuestsCreate } from '../../components/Quests';
import { QuestionsList, QuestionsCreate } from '../../components/Questions';
import { QuestionAreaList, QuestionAreaCreate } from '../../components/QuestionArea';
import { QuestionTypeList, QuestionTypeCreate } from '../../components/QuestionType';
import { HintsList, HintsCreate } from '../../components/Hints';
import { StaffList, StaffCreate } from '../../components/Staff';
import { StaffCategoryList, StaffCategoryCreate } from '../../components/StaffCategory';
import { ImagesList, ImagesCreate } from '../../components/Images';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

interface MatchParams {
  page_type: string;
  page_function: string;
}

interface PageWrapperProps extends RouteComponentProps<MatchParams> {
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(10, 3),
    },
  }),
);

const PageWrapper: React.FC<PageWrapperProps> = ({ match }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchGetQuestsAction.request());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchPermissionsAction.request());
  }, [dispatch]);

  const classes = useStyles();
  const page_type = match.params.page_type;
  const page_function = match.params.page_function;
  let content;
  switch (page_type) {
    case 'users':
      content = page_function === 'list' ? <UsersList /> : <UsersCreate />;
      break;
    case 'quests':
      content = page_function === 'list' ? <QuestsList /> : <QuestsCreate />;
      break;
    case 'questions':
      content = page_function === 'list' ? <QuestionsList /> : <QuestionsCreate />;
      break;
    case 'hints':
      content = page_function === 'list' ? <HintsList /> : <HintsCreate />;
      break;
    case 'staff':
      content = page_function === 'list' ? <StaffList /> : <StaffCreate />;
      break;
    case 'staffcategory':
      content = page_function === 'list' ? <StaffCategoryList /> : <StaffCategoryCreate />;
      break;
    case 'questionarea':
      content = page_function === 'list' ? <QuestionAreaList /> : <QuestionAreaCreate />;
      break;
    case 'questiontype':
      content = page_function === 'list' ? <QuestionTypeList /> : <QuestionTypeCreate />;
      break;
    case 'images':
      content = page_function === 'list' ? <ImagesList /> : <ImagesCreate />;
      break;
    default:
      content = 'page';
      break;
  }

  return (
    <main className={classes.content}>
      <section className="content-header">
        <Breadcrumbs aria-label="breadcrumb">
          <Link color="inherit" href="/" onClick={() => { }}>
            Main page</Link>
          <Typography color="textPrimary">{page_type} {page_function}</Typography>
        </Breadcrumbs>
      </section>
      <br />
      <section className="content">{content}</section>
    </main>
  );
};

export default withRouter(PageWrapper);