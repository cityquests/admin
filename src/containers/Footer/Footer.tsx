import React from 'react';
// import { withRouter, Redirect } from 'react-router-dom';
// import { useSelector, useDispatch } from 'react-redux';
// import { fetchGetUserAction } from '../../actions';

interface FooterProps {
  location?: any;
};

const Footer: React.FC<FooterProps> = ({ location }) => {
  // const dispatch = useDispatch();

  return (
    <footer className="main-footer">
      <div className="pull-right hidden-xs">
        <b>Версия</b> 1.0.0
      </div>
      <strong>Copyright &copy; 2017 <a href="/">ИП KARATAYEV</a>.</strong> All rights
        reserved.
    </footer>
  );
};

// export default withRouter(Footer);
export default Footer;