export const getTimeMessage = (createdAt: number) => {
  const diff = new Date().getTime() / 1000 - createdAt;
  const day = Math.floor(diff / 86400);
  if (day > 0) {
    return `${day}d`;
  } else {
    const hour = Math.floor((diff - day * 86400) / 3600);
    if (hour > 0) {
      return `${hour}h`;
    } else {
      return `${Math.ceil(diff / 60)}m`;
    }
  }
};